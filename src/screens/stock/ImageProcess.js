import React, { Component } from 'react';
import {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback, Image, Picker,
    SafeAreaView, ScrollView
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { ErrorMessage, Formik } from "formik";
import { Button, Input } from "react-native-elements";
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount,
    fetchAccountInfo,
    postLogin,
    uploadingImage,
    uploadImage,
    postNewStockProduct,
    fetchSteelTypeList,
    fetchStockGroupList,
    fetchStockProductStandardList,
    fetchWarehouseSelectionList, clearImageUpload, clearStockProductDetail
} from "../../../redux/actions/ActionCreators";
import { connect } from "react-redux";
import { Loading } from "../../components/Loading";
import { baseUrl } from "../../../shared/baseUrl";
import DatePicker from 'react-native-datepicker';
import * as Yup from "yup";
import moment from "moment";
// import { Picker } from '@react-native-community/picker';

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        addedSuccessStatus: state.stockDetail.addedSuccessStatus,
        errMess: state.stockDetail.errMess,
        addedStockProductId: state.stockDetail.addedStockProductId,
        steelInfos: state.steelInfos,
        warehouseSelections: state.warehouseSelections
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        postNewStockProduct: (values) => {
            dispatch(postNewStockProduct(values))
        },
        fetchSteelTypeList: () => {
            dispatch(fetchSteelTypeList())
        },
        fetchStockGroupList: () => {
            dispatch(fetchStockGroupList())
        },
        fetchStockProductStandardList: () => {
            dispatch(fetchStockProductStandardList())
        },
        fetchWarehouseSelectionList: () => {
            dispatch(fetchWarehouseSelectionList())
        },
        clearImageUpload: () => {
            dispatch(clearImageUpload());
        },
        clearStockProductDetail: () => {
            dispatch(clearStockProductDetail())
        }
    };
};
const AddStockSchema = Yup.object().shape({
    sku: Yup.string()
        .required('Required.')
        .matches(/^[A-Za-z0-9-*]{1,20}$/, "Invalid SKU or Invalid length."),
    barcode: Yup.string()
        .required('Required.')
        .matches(/^[0-9]{12}$/, "Invalid Barcode or Barcode not equal to 12 numbers."),
    name: Yup.string()
        .required('Required.')
        .matches(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]*$/, "Invalid Stock Product Name"),
    price: Yup.string()
        .required('Required.')
        .matches(/^[0-9]{1,10}$/i, "Invalid Price"),
    length: Yup.string()
        .required('Required.')
        .matches(/^[0-9]{1,5}$/i, "Invalid Length"),
});

class ImageProcess extends Component {

    getCurrentDate = () => {

        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();

        return date + '-' + month + '-' + year;
    }

    state = {
        account: null,
        type: null,
        stockGroupId: null,
        standard: null,
        warehouseId: null,
        manufacturedDate: this.getCurrentDate(),
        firstLoad: false,
        firstLoadType: false
    }

    constructor(props) {
        super(props);

        const steelInfos = this.props.steelInfos;
        const warehouseSelections = this.props.warehouseSelections;

        // if (steelInfos.steelTypeList.length === 0 && !steelInfos.isSteelTypeLoaded) {
        this.props.fetchSteelTypeList();
        // }
        // if (steelInfos.stockGroupList.length === 0 && !steelInfos.isStockGroupLoaded) {
        this.props.fetchStockGroupList();
        // }
        // if (steelInfos.stockProductStandardList.length === 0 && !steelInfos.isStockProductStandardLoaded) {
        this.props.fetchStockProductStandardList();
        // }
        // if (warehouseSelections.selectionList.length === 0 && !warehouseSelections.isSteelTypeLoaded) {
        this.props.fetchWarehouseSelectionList();
        // }

    }

    handleAddStock = async (values) => {
        await this.props.postNewStockProduct(values);
    }


    mapSteelTypeListToOption = (list) => {
        return list.map((item) => (
            <Picker.Item label={item.name} value={item.steeltypeId} key={item.steeltypeId}
            />
        ));
    };

    mapStockGroupListToOption = (list) => {
        return list.map((item) => (
            <Picker.Item label={item.name} value={item.stockProductGroupId} key={item.stockProductGroupId} />
        ));
    };

    mapStockProductStandardListToOption = (list) => {
        return list.map((item) => (
            <Picker.Item label={item.name} value={item.stockproductstandardId} key={item.stockproductstandardId} />

        ));
    };

    mapWarehouseToOption = () => {
        return this.props.warehouseSelections.selectionList.map((warehouseObj) => (
            <Picker.Item label={warehouseObj.name} value={warehouseObj.warehouseId} key={warehouseObj.warehouseId} />

        ));
    };

    async componentDidMount() {
        console.log("check props image process");
        console.log(this.props);
        await isValidSession(this.props);
        await this.props.clearImageUpload();
        await this.props.clearStockProductDetail();
    }

    render() {
        if (this.state.firstLoad === false) {
            if (this.props.steelInfos.steelTypeList !== null && this.props.steelInfos.steelTypeList !== undefined) {
                this.setState({
                    type: this.props.steelInfos.steelTypeList[0].steeltypeId,
                    stockGroupId: this.props.steelInfos.stockGroupList[0].stockProductGroupId,
                    standard: this.props.steelInfos.stockProductStandardList[0].stockproductstandardId,
                    warehouseId: this.props.warehouseSelections.selectionList[0].warehouseId,
                    firstLoad: true
                })
            }
        }





        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login />
        }

        console.log("Image Process")
        console.log(this.props.imageUpload);

        // let imageUpload;
        // if (this.props.imageUpload !== null &&
        //     this.props.imageUpload !== undefined) {
        //     imageUpload = this.props.imageUpload.stockProduct;
        //     if (imageUpload === undefined || stockDetail === null)
        //         return <Loading />
        //
        //     console.log("stockDetail");
        // } else
        //     return <Loading />

        console.log("dataInsert in ImageProcess")

        let dataToInsert = this.props.navigation.getParam("dataToInsert", null);

        console.log("dataToInsert ImageProcess");
        console.log(dataToInsert);

        if (dataToInsert === null) {
            return <Loading />
        }

        if (this.state.firstLoadType === false) {
            if (dataToInsert !== null && dataToInsert !== undefined && dataToInsert !== null && dataToInsert.type !== undefined) {
                this.setState({
                    type: dataToInsert.type.steeltypeId,
                    firstLoadType: true
                })
            }
        }

        // this.props.clearImageUpload();

        console.log("imageUpload aaaa");
        console.log(this.props.imageUpload);

        const addedSuccessStatus = this.props.addedSuccessStatus;
        const addedStockProductId = this.props.addedStockProductId;
        const successMessage = addedSuccessStatus ?
            <Text style={{ color: "green" }}>Added successfully!
                Click&nbsp;<Text
                    style={{ color: 'blue', textDecorationLine: 'underline' }}
                    onPress={() => this.props.navigation.navigate('StockDetail', { stockDetailId: addedStockProductId })}
                >
                    here
                </Text> to view stock product detail.
            </Text>
            : <Text></Text>;

        const errMess = this.props.errMess;

        const initialValues = {
            sku: dataToInsert.sku !== null && dataToInsert.sku !== undefined ? dataToInsert.sku : "",
            barcode: dataToInsert.barcode !== null && dataToInsert.barcode !== undefined ? dataToInsert.barcode : "",
            name: dataToInsert.name !== null && dataToInsert.name !== undefined ? dataToInsert.name : "",
            price: "",
            length: dataToInsert.length !== null && dataToInsert.length !== undefined ? "" + dataToInsert.length : "",
            manufacturedDate: this.getCurrentDate(),
            statusId: "SPS001",
            type: dataToInsert.type !== null && dataToInsert.type !== undefined && dataToInsert.type.steeltypeId !== null ? dataToInsert.type.steeltypeId : "",
            stockGroupId: "",
            standard: "",
            warehouseId: "",
        };

        console.log("initialValues");
        console.log(initialValues);

        return (
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Add New Stock</Text>
                </View>
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Formik
                        initialValues={
                            initialValues
                        }
                        validationSchema={AddStockSchema}
                        onSubmit={
                            values => {
                                values.type = this.state.type;
                                values.stockGroupId = this.state.stockGroupId;
                                values.standard = this.state.standard;
                                values.warehouseId = this.state.warehouseId;

                                let str = this.state.manufacturedDate;
                                let stringArr = str.split("");
                                let tempArr = str.split("");
                                stringArr.splice(0, 2, tempArr[3], tempArr[4]);
                                stringArr.splice(3, 2, tempArr[0], tempArr[1]);
                                stringArr.splice(5, 5);

                                let temp = "" + tempArr[6] + tempArr[7] + tempArr[8] + tempArr[9] + "-";
                                for (let i = 0; i < stringArr.length; i++) {
                                    temp += stringArr[i];
                                }

                                console.log("TEMPPPPPPPP1");
                                console.log(temp);


                                console.log("TEMPPPPPPPP");
                                console.log(temp);

                                values.manufacturedDate = temp;


                                console.log("VALUESSSSSSSSSSSS");
                                console.log(values);
                                this.handleAddStock(values)
                            }
                        }
                    >
                        {({ handleChange, handleBlur, handleSubmit, values }) => (
                            <View>
                                <Text>SKU
                                </Text>
                                <Input
                                    onChangeText={handleChange('sku')}
                                    onBlur={handleBlur('sku')}
                                    value={values.sku}
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="sku" /></Text>
                                </View>
                                <Text>Bar Code
                                </Text>
                                <Input
                                    onChangeText={handleChange('barcode')}
                                    onBlur={handleBlur('barcode')}
                                    value={values.barcode}
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="barcode" /></Text>
                                </View>
                                <Text>Product Name
                                </Text>
                                <Input
                                    onChangeText={handleChange('name')}
                                    onBlur={handleBlur('name')}
                                    value={values.name}
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="name" /></Text>
                                </View>
                                <Text>Price
                                </Text>
                                <Input
                                    onChangeText={handleChange('price')}
                                    onBlur={handleBlur('price')}
                                    value={values.price}
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="price" /></Text>
                                </View>
                                <Text>Length
                                </Text>
                                <Input
                                    onChangeText={handleChange('length')}
                                    onBlur={handleBlur('length')}
                                    value={values.length}
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="length" /></Text>
                                </View>
                                <Text>Manufactured Date
                                </Text>
                                {/* <View style={styles.dateSelect}> */}
                                <DatePicker
                                    style={{ width: 350 }}
                                    date={this.state.manufacturedDate}
                                    mode="date"
                                    placeholder="Select Date"
                                    format="DD-MM-YYYY"
                                    minDate="01-01-1970"
                                    maxDate={this.getCurrentDate()}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            //display: 'none',
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0,
                                        },
                                        dateInput: {
                                            marginLeft: 36,
                                        },
                                    }}
                                    onDateChange={(itemValue, itemIndex) => {
                                        console.log("DATEEEEEE");
                                        console.log(itemValue);
                                        this.setState({
                                            manufacturedDate: itemValue
                                        })
                                    }}
                                />

                                {/* </View> */}

                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="manufacturedDate" /></Text>
                                </View>
                                <Text>Status
                                </Text>
                                {/* <DropDownPicker
                                    items={[
                                        { label: 'Good', value: 'Good' },
                                        { label: 'Medium', value: 'Medium' },
                                        { label: 'Bad', value: 'Bad' }
                                    ]}
                                    defaultIndex={1}
                                    containerStyle={{ height: 40 }}
                                    onChangeItem={handleChange('status')}
                                /> */}

                                <Picker
                                    selectedValue={values.statusId}
                                    // style={{ height: 50, width: 100 }}
                                    onValueChange={handleChange('statusId')}>
                                    <Picker.Item label="Good" value="SPS001" />
                                    <Picker.Item label="Medium" value="SPS002" />
                                    <Picker.Item label="Bad" value="SPS003" />
                                </Picker>

                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="statusId" /></Text>
                                </View>
                                <Text>Type
                                </Text>

                                <Picker
                                    selectedValue={this.state.type}
                                    // style={{ height: 50, width: 100 }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({
                                            type: itemValue
                                        })
                                    }}>
                                    {this.mapSteelTypeListToOption(this.props.steelInfos.steelTypeList, this.state.type)}
                                </Picker>


                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="type" /></Text>
                                </View>
                                <Text>Stock Group
                                </Text>
                                <Picker
                                    selectedValue={this.state.stockGroupId}
                                    // style={{ height: 50, width: 100 }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({
                                            stockGroupId: itemValue
                                        })
                                    }}>
                                    {this.mapStockGroupListToOption(this.props.steelInfos.stockGroupList)}
                                </Picker>


                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="stockGroupId" /></Text>
                                </View>
                                <Text>Standard
                                </Text>
                                <Picker
                                    selectedValue={this.state.standard}
                                    // style={{ height: 50, width: 100 }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({
                                            standard: itemValue
                                        })
                                    }}>
                                    {this.mapStockProductStandardListToOption(this.props.steelInfos.stockProductStandardList)}
                                </Picker>

                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="standard" /></Text>
                                </View>
                                <Text>Warehouse Id
                                </Text>
                                <Picker
                                    selectedValue={this.state.warehouseId}
                                    // style={{ height: 50, width: 100 }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({
                                            warehouseId: itemValue
                                        })
                                    }}>
                                    {this.mapWarehouseToOption(this.props.warehouseSelections.selectionList)}
                                </Picker>

                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="warehouseId" /></Text>
                                </View>
                                {
                                    errMess &&
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ color: 'red' }}><Text>{errMess}</Text></Text>
                                    </View>
                                }
                                {
                                    addedSuccessStatus &&
                                    <View style={{ marginBottom: 10 }}>
                                        {successMessage}
                                    </View>
                                }
                                <Button buttonStyle={{ backgroundColor: "#ffa800" }} onPress={handleSubmit}
                                    title="Submit" />
                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 30
    },
    // dateSelect: {
    //     flexDirection: 'row',
    // },
});

export default connect(mapStateToProps, mapDispatchToProps)(
    ImageProcess
);
