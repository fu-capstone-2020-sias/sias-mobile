import React, {Component} from 'react';
import {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView, Platform
} from 'react-native';
import {Table, Row, Rows} from 'react-native-table-component';
import {createStackNavigator} from "react-navigation-stack";
import {connect} from "react-redux";
import {createAppContainer} from "react-navigation";
import {clearLoginCredentialsExceptAccount, fetchStockProduct, postLogin} from "../../../redux/actions/ActionCreators";
import StockDetail from "./StockDetail";
import Login from "../account/Login";
import {Loading} from "../../components/Loading";
import Button, {DataTable} from 'react-native-paper';
import {Searchbar} from 'react-native-paper';
import NumberFormat from "react-number-format";

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        stocks: state.stocks,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchStockProduct: (pageNumber) => {
            dispatch(fetchStockProduct(pageNumber))
        },
        fetchStockProductSearch: (pageNumber, searchData) => {
            dispatch(fetchStockProduct(pageNumber, searchData))
        },
    };
};

class StockManagement extends Component {

    state = {
        searchString: ""
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        const pageNumber = 1;

        await this.props.fetchStockProduct(pageNumber);
    }

    mapStockToRow = (stocks) => {
        const stocksMapped = stocks.map((stockObj, row) => {
            const zebraStyle = row % 2 !== 0 ? {backgroundColor: '#c8cbce'} : {}

            return (
                <DataTable.Row style={zebraStyle}>
                    <DataTable.Cell>{stockObj.stockProductId}</DataTable.Cell>
                    <DataTable.Cell>{stockObj.name}</DataTable.Cell>
                    <DataTable.Cell>{stockObj.sku}</DataTable.Cell>
                    <DataTable.Cell>{stockObj.barcode}</DataTable.Cell>
                    <DataTable.Cell>{stockObj.steeltype.name}</DataTable.Cell>
                    <DataTable.Cell>
                        <NumberFormat value={stockObj.length} displayType={'text'}
                                      thousandSeparator={true} renderText={value =>
                            <Text>{value}</Text>}/>
                    </DataTable.Cell>
                    <DataTable.Cell>{stockObj.manufacturedDate}</DataTable.Cell>
                    <DataTable.Cell>{stockObj.warehouse.name}</DataTable.Cell>
                    <DataTable.Cell>
                        {this.mapStatusColor(stockObj.stockproductstatus.name)}
                    </DataTable.Cell>
                    <DataTable.Cell>
                        <NumberFormat value={stockObj.price} displayType={'text'}
                                      thousandSeparator={true} renderText={value =>
                            <Text>{value}</Text>}/>
                    </DataTable.Cell>
                    <DataTable.Cell
                        onPress={() => this.props.navigation.navigate('StockDetail', {stockDetailId: stockObj.stockProductId})}>
                        <Text style={{color: 'blue', textDecorationLine: 'underline'}}>
                            Details
                        </Text>
                    </DataTable.Cell>
                    {/* <DataTable.Cell
                        onPress={() => this.props.navigation.navigate('')}>
                        <Text style={{color: 'blue', textDecorationLine: 'underline'}}>
                            Delete
                        </Text>
                    </DataTable.Cell> */}
                </DataTable.Row>
            )
        })

        return stocksMapped;
    }

    mapStatusColor = (status) => {
        switch (status) {
            case 'Good':
                return <Text style={{backgroundColor: '#28a745'}}>{status}</Text>
            case 'Medium':
                return <Text style={{backgroundColor: '#17a2b8'}}>{status}</Text>
            case 'Bad':
                return <Text style={{backgroundColor: '#dc3545'}}>{status}</Text>
        }
    }

    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        let stocks;
        if (this.props.stocks !== null &&
            this.props.stocks !== undefined) {
            stocks = this.props.stocks.stockProducts;
            if (stocks === undefined || stocks === null)
                return <Loading/>

            console.log("check stocks");
            console.log(stocks);
        } else
            return <Loading/>

        return (
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>View Stock List</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Searchbar
                        placeholder="Search by Stock ID"
                        onIconPress={async () => {
                            await this.props.fetchStockProductSearch(1,
                                {
                                    stockProductId: this.state.searchString
                                })
                        }}
                        onSubmitEditing={async () => {
                            await this.props.fetchStockProductSearch(1,
                                {
                                    stockProductId: this.state.searchString
                                })
                        }}
                        onChangeText={(query) => {
                            console.log("QUERRRRYYYYYYYYYY");
                            console.log(query);
                            this.setState({
                                searchString: query
                            })
                        }}
                        value={this.state.searchString}
                    />
                </View>
                <View style={styles.formContainer}>
                    <ScrollView horizontal={true}>
                        <DataTable
                            style={{width: 1400}}
                        >

                            <DataTable.Header style={{backgroundColor: "#ffb74d"}}>
                                <DataTable.Title>ID</DataTable.Title>
                                <DataTable.Title>Name</DataTable.Title>
                                <DataTable.Title>SKU</DataTable.Title>
                                <DataTable.Title>Bar code</DataTable.Title>
                                <DataTable.Title>Type</DataTable.Title>
                                <DataTable.Title>Length (mm)</DataTable.Title>
                                <DataTable.Title>Manufactured Date</DataTable.Title>
                                <DataTable.Title>Warehouse</DataTable.Title>
                                <DataTable.Title>Status</DataTable.Title>
                                <DataTable.Title>Price (VNĐ)</DataTable.Title>
                                <DataTable.Title></DataTable.Title>
                                {/* <DataTable.Title></DataTable.Title> */}
                            </DataTable.Header>

                            {this.mapStockToRow(stocks)}
                        </DataTable>
                    </ScrollView>
                    <Text style={{marginTop: 20}}>Total records: {this.props.stocks.totalEntries}</Text>
                    <DataTable.Pagination
                        page={this.props.stocks.currentPageNumber}
                        numberOfPages={this.props.stocks.numberOfPages + 1}
                        onPageChange={async pageNumber => {
                            console.log(pageNumber)
                            console.log(isNaN(pageNumber));

                            if (pageNumber !== 0) {
                                if (this.state.searchString) {
                                    await this.props.fetchStockProductSearch(pageNumber,
                                        {
                                            stockProductId: this.state.searchString
                                        })
                                    return;
                                }
                                await this.props.fetchStockProduct(pageNumber)
                            }
                        }}
                    />
                </View>
            </ScrollView>
        )
    }
}

const navigator = createStackNavigator(
    {
        StockManagement: {
            screen: connect(mapStateToProps, mapDispatchToProps)(StockManagement)
        },
        StockDetail: {
            screen: StockDetail
        },

    },
    {
        initialRouteName: "StockManagement",
        defaultNavigationOptions:
            {
                headerShown: false
            }
    },
);


const Container = createAppContainer(navigator);

class StockManagementNavigateToStockDetail extends Component {
    componentDidMount() {

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    paddingTop:
                        Platform.OS === "ios" ? 0 : Expo.Constants.statusBarHeight,
                }}
            >
                <Container/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 0,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(StockManagementNavigateToStockDetail);
