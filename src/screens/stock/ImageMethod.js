import React, {Component} from 'react';
import {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback, Image,
    SafeAreaView, ScrollView, Picker
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, fetchAccountInfo,
    postLogin, uploadingImage, uploadImage, clearImageUpload
} from "../../../redux/actions/ActionCreators";
import {createStackNavigator} from "react-navigation-stack";
import {connect} from "react-redux";
import {createAppContainer} from "react-navigation";
import ImageProcess from "./ImageProcess";
import {Loading} from "../../components/Loading";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import {baseUrl} from "../../../shared/baseUrl";
import StockDetail from "./StockDetail";
import {
    LABEL_IMAGE_TYPE_BCP,
    LABEL_IMAGE_TYPE_EN,
    LABEL_IMAGE_TYPE_NIKKEN, LABEL_IMAGE_TYPE_NIKKEN_DOC,
    LABEL_IMAGE_TYPE_NSMP
} from "../../../shared/constants";

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        imageUpload: state.imageUpload
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        uploadImage: (formData) => {
            dispatch(uploadImage(formData));
        },
        uploadingImage: () => {
            dispatch(uploadingImage())
        },
        clearImageUpload: () => {
            dispatch(clearImageUpload());
        }
    };
};

class ImageMethod extends Component {
    state = {
        account: null,
        imageUrl: baseUrl,
        isImageUploaded: false,
        labelType: "Default"
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);
        // await this.props.clearImageUpload();
    }

    getImageFromCamera = async () => {
        if (this.state.labelType === "Default") {
            return;
        }

        await this.props.clearImageUpload();

        const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
        const cameraRollPermission = await Permissions.askAsync(
            Permissions.CAMERA_ROLL
        );

        if (
            cameraPermission.status === "granted" &&
            cameraRollPermission.status === "granted"
        ) {
            let capturedImage = await ImagePicker.launchCameraAsync({
                // allowsEditing: true,
                aspect: [16, 9],
            });


            console.log("capturedImage");
            console.log(capturedImage);

            if (!capturedImage.cancelled) {
                this.setState({
                    imageUrl: capturedImage.uri,
                });
                // await this.imageToForm();
                await this.props.uploadingImage();
                const formData = new FormData();
                // formData.append('image', this.state.imageUrl);
                formData.append('option', this.state.labelType);
                formData.append("image", {
                        uri: capturedImage.uri,
                        name: `photo.png`,
                        type: `image/jpeg`
                    }
                )

                console.log("formData vjp");
                console.log(formData);
                await this.props.uploadImage(formData);

                this.setState({
                    isImageUploaded: true
                })

                console.log("imageUpload");
            }
        }
    };

    getImageFromGallery = async () => {
        if (this.state.labelType === "Default") {
            return;
        }

        await this.props.clearImageUpload();
        const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
        const cameraRollPermission = await Permissions.askAsync(
            Permissions.CAMERA_ROLL
        );

        if (
            cameraPermission.status === "granted" &&
            cameraRollPermission.status === "granted"
        ) {
            let capturedImage = await ImagePicker.launchImageLibraryAsync({
                // allowsEditing: true,
                // aspect: [16, 9],
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                quality: 1,
            });

            console.log("capturedImage");
            console.log(capturedImage);

            if (!capturedImage.cancelled) {
                this.setState({
                    imageUrl: capturedImage.uri,
                });
                // await this.imageToForm();
                await this.props.uploadingImage();
                const formData = new FormData();
                // formData.append('image', this.state.imageUrl);
                formData.append('option', this.state.labelType);
                formData.append("image", {
                        uri: capturedImage.uri,
                        name: `photo.png`,
                        type: `image/jpeg`
                    }
                )
                await this.props.uploadImage(formData);

                this.setState({
                    isImageUploaded: true
                })

                console.log("imageUpload");
            }
        }
    };

    // imageToForm = async () => {
    //     if (imageUrl == baseUrl) {
    //         //Noti
    //         return;
    //     }
    //     await props.uploadingImage();
    //     const formData = new FormData();
    //     formData.append('image', imageUrl);
    //     await props.uploadImage(formData);

    //     console.log("imageUpload");
    // }
    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        let checkError = false;
        let dataToInsert;
        if (this.props.imageUpload !== null && this.props.imageUpload !== undefined) {
            dataToInsert = this.props.imageUpload.dataToInsert;
            console.log("dataToInsert lolo");
            console.log(dataToInsert);

            if (dataToInsert === null && this.state.isImageUploaded
            ) {
                return <Loading message={"Uploading image for OCR..."}/>
            }

            console.log("dataToInsert !== null && Object.keys(dataToInsert).length === 0 && dataToInsert.constructor === Object && this.state.isImageUploaded");
            console.log(dataToInsert !== null && Object.keys(dataToInsert).length === 0 && dataToInsert.constructor === Object && this.state.isImageUploaded)

            let skip = false;
            if (dataToInsert !== null && Object.keys(dataToInsert).length === 0 && dataToInsert.constructor === Object && this.state.isImageUploaded) {
                skip = true;
                // this.setState({
                //     isImageUploaded: false
                // })
                // this.props.clearImageUpload();
                checkError = true;
            }

            if (dataToInsert !== null && this.state.isImageUploaded && skip === false
            ) {
                this.props.navigation.navigate('ImageProcess', {dataToInsert: dataToInsert})

                this.setState({
                    isImageUploaded: false
                })
            }
        }


        console.log("checkError");
        console.log(checkError);

        return (
            <View style={styles.formContainer}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Add New Stock Product</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                {
                    checkError ?
                        <View style={styles.titleContainer}>
                            <Text style={{color: "red"}}>Error: Unable to read data from label image. Make sure you are
                                uploading the correct image file.</Text>
                        </View> : <Text></Text>
                }
                <View style={{
                    marginTop: 50,
                    alignItems: 'center',
                    flex: 1,
                    justifyContent: 'center'
                }}>
                    <Text>Choose label type below <Text style={{color: "red"}}>(required)</Text>:</Text>
                    <Picker
                        style={{width: 200}}
                        selectedValue={this.state.labelType}
                        onValueChange={(itemValue) => {
                            if (itemValue !== "Default")
                                this.setState({
                                    labelType: itemValue
                                })
                        }}>
                        <Picker.Item label="Choose..." value={"Default"}/>
                        <Picker.Item label="NSMP" value={LABEL_IMAGE_TYPE_NSMP}/>
                        <Picker.Item label="EN" value={LABEL_IMAGE_TYPE_EN}/>
                        <Picker.Item label="BCP" value={LABEL_IMAGE_TYPE_BCP}/>
                        <Picker.Item label="NIKKEN" value={LABEL_IMAGE_TYPE_NIKKEN}/>
                        <Picker.Item label="NIKKEN-DOC" value={LABEL_IMAGE_TYPE_NIKKEN_DOC}/>
                    </Picker>
                </View>
                <View style={styles.infoContainer}>
                    <TouchableOpacity onPress={this.getImageFromCamera}>
                        <View style={styles.methodContainer}>
                            <Image
                                source={require('../../../assets/ImageMethod/icon_imgCapture.png')}
                                style={styles.logoImage}
                            />
                            <Text style={styles.methodText}>Capture Image</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.getImageFromGallery}>
                        <View style={styles.methodContainer}>
                            <Image
                                source={require('../../../assets/ImageMethod/icon_imgGallery.png')}
                                style={styles.logoImage}
                            />
                            <Text style={styles.methodText}>Upload Image</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {/*/!* Test Ảnh *!/*/}
                {/*<View>*/}
                {/*    <Image*/}
                {/*        source={{uri: this.state.imageUrl}}*/}
                {/*        style={styles.logoImage}*/}
                {/*    />*/}
                {/*</View>*/}
            </View>
        )
    }
}


const navigator = createStackNavigator(
    {
        ImageMethod: {
            screen: connect(mapStateToProps, mapDispatchToProps)(ImageMethod)
        },
        ImageProcess: {
            screen: ImageProcess
        },
        StockDetail: {
            screen: StockDetail
        },
    },
    {
        initialRouteName: "ImageMethod",
        defaultNavigationOptions:
            {
                headerShown: false
            }
    },
);


const Container = createAppContainer(navigator);

class ImageMethodNavigateToImageProcess extends Component {
    componentDidMount() {

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    paddingTop:
                        Platform.OS === "ios" ? 0 : Expo.Constants.statusBarHeight,
                }}
            >
                <Container/>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        paddingLeft: 10,
        paddingRight: 10,

    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    logoImage: {
        height: 120,
        width: 120
    },
    methodText: {
        textAlign: 'center',
        alignItems: 'center',
        fontSize: 14,
        color: 'black',
        marginTop: 5,
        fontWeight: 'bold'

    },
    infoContainer: {
        paddingTop: 100,
        flexDirection: "row",
        alignContent: 'center',
    },
    methodContainer: {
        padding: 20,
        paddingLeft: 30,
        borderColor: 'black',
    },
});


export default connect(mapStateToProps, mapDispatchToProps)(
    ImageMethodNavigateToImageProcess
);
