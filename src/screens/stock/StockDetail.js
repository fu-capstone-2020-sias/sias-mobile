import React, {Component} from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {Button, Input} from "react-native-elements";
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, fetchAccountInfo, fetchStockProductDetail,
    postLogin
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import {Loading} from "../../components/Loading";
import {Card, DataTable} from 'react-native-paper';
import NumberFormat from "react-number-format";
// import NumberFormat from 'react-number-format';

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        stockDetail: state.stockDetail,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchStockProductDetail: (stockProductId) => {
            dispatch(fetchStockProductDetail(stockProductId))
        },
    };
};


class StockDetail extends Component {
    state = {
        account: null
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        await this.props.fetchStockProductDetail(this.props.navigation.getParam("stockDetailId", ""));
    }

    mapStatusColor = (status) => {
        switch (status) {
            case 'Good':
                return <Text style={{backgroundColor: '#28a745'}}>{status}</Text>
            case 'Medium':
                return <Text style={{backgroundColor: '#17a2b8'}}>{status}</Text>
            case 'Bad':
                return <Text style={{backgroundColor: '#dc3545'}}>{status}</Text>
        }
    }


    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        console.log("Stock Detail")
        console.log(this.props.stockDetail);

        let stockDetail;
        if (this.props.stockDetail !== null &&
            this.props.stockDetail !== undefined) {
            stockDetail = this.props.stockDetail.stockProduct;
            if (stockDetail === undefined || stockDetail === null)
                return <Loading/>

            console.log("stockDetail");
        } else
            return <Loading/>

        return (
            // Tạm thời sẽ thêm scrollview + nếu Accordion ok thì thêm vào cho đẹp và gọn hơn
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>View Stock Detail</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Card>
                        <Card.Title title="Stock Details"/>
                        <Card.Content>
                            {/* START: Customer Section */}
                            <ScrollView horizontal={true}>
                                <DataTable
                                    style={{width: 500}}
                                >
                                    <DataTable.Row>
                                        <DataTable.Cell>Name:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.name}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Stock Product ID:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.stockProductId}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>SKU:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.sku}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Bar Code:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.barcode}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Steel Type:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.steeltype.name}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Stock Product Group:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.stockproductgroup.name}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Standard:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.stockproductstandard.name}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Status:</DataTable.Cell>
                                        <DataTable.Cell>{this.mapStatusColor(stockDetail.stockproductstatus.name)}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Price (VNĐ):</DataTable.Cell>
                                        <DataTable.Cell>
                                            <NumberFormat value={stockDetail.price} displayType={'text'}
                                                          thousandSeparator={true} renderText={value =>
                                                <Text>{value}</Text>}/>
                                        </DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Length (mm):</DataTable.Cell>
                                        <DataTable.Cell>
                                            <NumberFormat value={stockDetail.length} displayType={'text'}
                                                          thousandSeparator={true} renderText={value =>
                                                <Text>{value}</Text>}/>
                                        </DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Manufactured Date:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.manufacturedDate}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Warehouse:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.warehouse.name}</DataTable.Cell>
                                    </DataTable.Row>

                                </DataTable>
                            </ScrollView>
                            {/* END: Customer Section */}
                        </Card.Content>
                    </Card>
                    <Card style={{marginTop: 20}}>
                        <Card.Title title="Stock Details"/>
                        <Card.Content>
                            {/* START: Customer Section */}
                            <DataTable
                                style={{width: 400}}
                            >
                                <DataTable.Row>
                                    <DataTable.Cell>Creator:</DataTable.Cell>
                                    <DataTable.Cell>{stockDetail.creator}</DataTable.Cell>
                                </DataTable.Row>

                                <DataTable.Row>
                                    <DataTable.Cell>Created Date:</DataTable.Cell>
                                    <DataTable.Cell>{stockDetail.createdDate}</DataTable.Cell>
                                </DataTable.Row>

                                <DataTable.Row>
                                    <DataTable.Cell>Updater:</DataTable.Cell>
                                    <DataTable.Cell>{stockDetail.updater ? stockDetail.updater : "N/A"}</DataTable.Cell>
                                </DataTable.Row>

                                <DataTable.Row>
                                    <DataTable.Cell>Updated Date:</DataTable.Cell>
                                    <DataTable.Cell>{stockDetail.updatedDate ? stockDetail.updatedDate : "N/A"}</DataTable.Cell>
                                </DataTable.Row>


                            </DataTable>
                            {/* END: Customer Section */}
                        </Card.Content>
                    </Card>

                    {/* <Card style={{marginTop: 20}}>
                        <Card.Content>
                            <ScrollView horizontal={true}>
                                <DataTable
                                    style={{width: 1400}}
                                >
                                    <DataTable.Row>
                                        <DataTable.Cell>Creator:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.creator}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Created Date:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.createdDate}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Updater:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.updater}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Updated Date:</DataTable.Cell>
                                        <DataTable.Cell>{stockDetail.updatedDate}</DataTable.Cell>
                                    </DataTable.Row>

                                    
                                </DataTable>
                            </ScrollView>
                        </Card.Content>
                    </Card> */}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(
    StockDetail
);
