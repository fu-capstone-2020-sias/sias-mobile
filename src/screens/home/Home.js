import React, {Component} from 'react';
import {
    Text, StyleSheet, View, TouchableOpacity,
    Image,
    SafeAreaView, ScrollView
} from 'react-native';
import {List} from 'react-native-paper';
import {createStackNavigator} from "react-navigation-stack";
import Login from "../account/Login";
import ViewAccount from "../account/Feedback";
import ChangePassword from "../account/ChangePassword";
import OrderManagement from "../order/OrderManagement";
import StockManagement from "../stock/StockManagement";
import Feedback from "../account/Feedback";
import {createAppContainer} from "react-navigation";
import {
    clearAccountInfoErrMess,
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, clearOrders,
    postLogin, postLogout
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import {ROLE_FACTORY_MANAGER} from "../../../shared/constants";
import {Loading} from "../../components/Loading";
//
// const navigator = createStackNavigator(
//     {
//         Login: Login,
//
//         Home: Home,
//         ViewAccount: ViewAccount,
//         ChangePassword: ChangePassword,
//
//         Order: OrderManagement,
//
//         Stock: StockManagement,
//
//         Feedback: Feedback,
//     },
//     {
//         initialRouteName: "Login",
//         defaultNavigationOptions:
//             {
//                 headerShown: false
//             }
//     },
// );
//
//
// const Container = createAppContainer(navigator);

import { YellowBox } from "react-native";
YellowBox.ignoreWarnings([""]);

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

class Home extends Component {

    state = {
        reRender: false
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        await this.props.clearAccountInfoErrMess();
    }

    render() {
        console.log("check props home");
        console.log(this.props)

        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        console.log("Check props navigation");
        console.log(this.props.navigation);

        console.log("Check account login");
        console.log(this.props.loginAccount.account);

        if (this.props.loginAccount.account !== null) {
            console.log("Check true of false");
            console.log(this.props.loginAccount.account.roleId !== ROLE_FACTORY_MANAGER);
        }

        if (this.props.loginAccount.account === null) {
            console.log("LOI 01");
            return <Login/>

            // if (this.state.reRender === true) {
            //     console.log("reRender");
            //     console.log(this.state.reRender);
            //     this.setState({
            //         reRender: false
            //     })
            // }
            // this.props.navigation.navigate("Login")
            // return <Loading/>
        }

        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.account.roleId !== ROLE_FACTORY_MANAGER) {
                console.log("LOI 02");
                this.props.clearOrders()
                this.props.postLogout()
                return <Login/>

                // this.props.navigation.navigate("Login")
                // return <Loading/>
            }
        }


        let firstName = null;
        let lastName = null;
        let fullName = null;

        if (this.props.loginAccount.account !== null) {
            firstName = this.props.loginAccount.account.firstName;
            lastName = this.props.loginAccount.account.lastName;
        }

        if (firstName !== null && lastName !== null) {
            fullName = firstName + " " + lastName;
        }

        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    {/* <StatusBar hidden={true}/> */}
                    <Text style={styles.title}>Welcome, <Text
                        style={{color: '#ffa800'}}>{fullName !== null ? fullName : ""}</Text></Text>
                    <ScrollView style={styles.listContainer}>
                        <List.Section>
                            <List.Accordion
                                titleStyle={styles.subTitle}
                                title="Account"
                                left={() => <Image source={require('../../../assets/Home/icon_account.png')}
                                                   style={styles.iconImage}/>}
                            >
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("ViewAccount")}><List.Item
                                    titleStyle={styles.containerText} title="View Profile"/></TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("ChangePassword")}><List.Item
                                    titleStyle={styles.containerText} title="Change Password"/></TouchableOpacity>
                                <TouchableOpacity onPress={async () => {
                                    await this.props.clearOrders();
                                    await this.props.postLogout()
                                }}><List.Item
                                    titleStyle={styles.logOut} title="Log Out"/></TouchableOpacity>
                            </List.Accordion>

                            <List.Accordion
                                titleStyle={styles.subTitle}
                                title="Order Management"
                                left={() => <Image source={require('../../../assets/Home/icon_order.png')}
                                                   style={styles.iconImage}/>}
                            >
                                <TouchableOpacity onPress={() => this.props.navigation.navigate("Order")}><List.Item
                                    titleStyle={styles.containerText} title="View Order List"/></TouchableOpacity>
                            </List.Accordion>

                            <List.Accordion
                                titleStyle={styles.subTitle}
                                title="Stock Management"
                                left={() => <Image source={require('../../../assets/Home/icon_stock.png')}
                                                   style={styles.iconImage}/>}
                            >
                                <TouchableOpacity onPress={() => this.props.navigation.navigate("Stock")}><List.Item
                                    titleStyle={styles.containerText} title="View Stock List"/></TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("ImageMethod")}><List.Item
                                    titleStyle={styles.containerText} title="Add New Stock Product"/></TouchableOpacity>
                            </List.Accordion>

                            <List.Accordion
                                titleStyle={styles.subTitle}
                                title="Feedback"
                                left={() => <Image source={require('../../../assets/Home/icon_feedback.png')}
                                                   style={styles.iconImage}/>}
                            >
                                <TouchableOpacity onPress={() => this.props.navigation.navigate("Feedback")}><List.Item
                                    titleStyle={styles.containerText} title="Send Feedback"/></TouchableOpacity>
                            </List.Accordion>
                        </List.Section>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        flexDirection: "column",
        padding: 10,
    },
    listContainer: {
        marginTop: 20,
    },
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    subTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        color: "black"
    },
    containerText: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    logOut: {
        fontWeight: 'bold',
        fontSize: 16,
        color: 'red'
    },
    iconImage: {
        height: 40,
        width: 40,
        marginRight: 10
        // borderRadius: 25
    },

});

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        },
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        clearAccountInfoErrMess: () => dispatch(clearAccountInfoErrMess()),
        postLogout: () => dispatch(postLogout()),
        clearOrders: () => dispatch(clearOrders())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
