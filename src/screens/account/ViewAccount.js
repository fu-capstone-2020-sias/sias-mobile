import React, {Component} from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {Button, Input} from "react-native-elements";
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, fetchAccountInfo,
    postLogin
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import Login from "./Login";
import {Loading} from "../../components/Loading";

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

class ViewAccount extends Component {
    state = {
        account: null
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        const account = this.props.loginAccount.account;
        console.log("account != null");
        console.log(account);
        if (account != null)
            await this.props.fetchAccountInfo(account.userName);
        this.setState({
            account: account,
        });

        console.log("componentDidMount");
        console.log(this.props.accountInfos)
    }

    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        let accountInfo;
        if (this.props.accountInfos !== null &&
            this.props.accountInfos !== undefined) {
            accountInfo = this.props.accountInfos.accountInfo;
            if (accountInfo === undefined || accountInfo === null)
                return <Loading/>

            console.log("accountInfo");
            console.log(accountInfo);
        } else
            return <Loading/>

        return (
            // Tạm thời sẽ thêm scrollview + nếu Accordion ok thì thêm vào cho đẹp và gọn hơn
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>View Profile</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Text style={{color: 'red'}}>
                        Note: If you want to change your account information, please send
                        a feedback to Administrator.
                    </Text>
                </View>
                <View style={styles.formContainer}>
                    <Formik
                        initialValues={
                            {
                                // userName: '',
                                // password: ''
                            }
                        }
                        onSubmit={
                            values => console.log("do nothing")
                        }
                    >
                        {({handleChange, handleBlur, handleSubmit, values}) => (
                            <View>
                                <Text>Username
                                </Text>
                                <Input
                                    value={accountInfo.userName}
                                    disabled={true}
                                />
                                <Text>First Name
                                </Text>
                                <Input
                                    value={accountInfo.firstName}
                                    disabled={true}
                                />
                                <Text>Last Name
                                </Text>
                                <Input
                                    value={accountInfo.lastName}
                                    disabled={true}
                                />
                                <Text>Gender
                                </Text>
                                <Input
                                    value={accountInfo.gender === "1" ? "Female" : "Male"}
                                    disabled={true}
                                />
                                <Text>Date of Birth
                                </Text>
                                <Input
                                    value={accountInfo.dob}
                                    disabled={true}
                                />
                                <Text>E-mail
                                </Text>
                                <Input
                                    value={accountInfo.email}
                                    disabled={true}
                                />
                                <Text>Phone Number
                                </Text>
                                <Input
                                    value={accountInfo.phoneNumber}
                                    disabled={true}
                                />
                                <Text>Role
                                </Text>
                                <Input
                                    value={accountInfo.role}
                                    disabled={true}
                                />
                                <Text>Department
                                </Text>
                                <Input
                                    value={accountInfo.department}
                                    disabled={true}
                                />
                                <Text>Address
                                </Text>
                                <Input
                                    value={accountInfo.address}
                                    disabled={true}
                                />
                                <Text>City/Province
                                </Text>
                                <Input
                                    value={accountInfo.cityProvince}
                                    disabled={true}
                                />

                                <Text>District
                                </Text>
                                <Input
                                    value={accountInfo.district}
                                    disabled={true}
                                />

                                <Text>Ward/Commune
                                </Text>
                                <Input
                                    value={accountInfo.wardCommune}
                                    disabled={true}
                                />

                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});


const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ViewAccount
);
