import React, {Component} from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {Button, Input} from "react-native-elements";
import {
    clearAccountInfoErrMess,
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, clearOrders, fetchAccountInfo, modifyAccountInfo,
    postLogin, postLogout
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import Login from "./Login";
import {Loading} from "../../components/Loading";
import * as Yup from "yup";

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

const changePasswordOk = (userName) => {
    return "Account with userName: " + userName + " updated."
}

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

// const password = (val) => /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(val)

const ChangePasswordSchema = Yup.object().shape({
    oldPassword: Yup.string()
        .required('Required.'),
    newPassword: Yup.string()
        .required('Required.')
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
            "Password must meet minimum " +
            "eight characters, at least one uppercase letter, " +
            "one lowercase letter, " +
            "one number and one special character (@, $, !, %, *, ?, &)."
        ),
});


class ChangePassword extends Component {
    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <Text style={{color: 'red'}}>{errMessObj}</Text>
        })
    }

    handleSubmit = async (values) => {
        await this.props.modifyAccountInfo(
            this.props.accountInfos.accountInfo.userName,
            values.newPassword,
            values.oldPassword,
            "1",
            this.props.accountInfos.accountInfo.roleId,
            this.props.accountInfos.accountInfo.departmentId,
            this.props.accountInfos.accountInfo.firstName,
            this.props.accountInfos.accountInfo.lastName,
            this.props.accountInfos.accountInfo.gender,
            this.props.accountInfos.accountInfo.dob,
            this.props.accountInfos.accountInfo.email,
            this.props.accountInfos.accountInfo.phoneNumber,
            this.props.accountInfos.accountInfo.cityProvinceId,
            this.props.accountInfos.accountInfo.districtId,
            this.props.accountInfos.accountInfo.wardCommuneId,
            this.props.accountInfos.accountInfo.address,
            this.props.accountInfos.accountInfo.disableFlag,
            "WEB_UC08"
        );
        await this.props.clearAccountInfoErrMess();
    };

    state = {
        firstLoad: false,
        visibleOldPassword: false,
        visibleNewPassword: false
    };

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        isValidSession(this.props);

        const account = this.props.loginAccount.account;
        await this.props.clearAccountInfoErrMess();
        this.setState({
            account: account,
        });

        if (null != account) {
            await this.props.fetchAccountInfo(account.userName); // Pass userName từ màn View Account List
        }
    }

    render() {

        if (this.props.loginAccount.account !== null) {

            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.accountInfos.successStatus) {
            if (this.props.accountInfos.successMessage != null && this.props.loginAccount.account !== null) {
                if (this.props.accountInfos.successMessage === changePasswordOk(this.props.loginAccount.account.userName)) {
                    this.props.clearLoginCredentials();
                    this.props.clearOrders();
                    this.props.clearAccountInfoErrMess();
                    this.props.postLogout();
                }
            }
        }

        // if (this.props.accountInfos.errMess !== null) {
        //     if (this.props.accountInfos.errMess === "Error 401: undefined") {
        //         this.props.clearLoginCredentials();
        //     }
        // }


        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        let accountInfo;
        if (this.props.accountInfos !== null &&
            this.props.accountInfos !== undefined) {
            accountInfo = this.props.accountInfos.accountInfo;
            console.log("accountInfo");
            console.log(accountInfo);
        } else
            return <Loading/>

        return (
            // Tạm thời sẽ thêm scrollview + nếu Accordion ok thì thêm vào cho đẹp và gọn hơn
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Change Password</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Formik
                        initialValues={
                            {
                                oldPassword: '',
                                newPassword: ''
                            }
                        }
                        validationSchema={ChangePasswordSchema}
                        onSubmit={
                            values => this.handleSubmit(values)
                        }
                    >
                        {({handleChange, handleBlur, handleSubmit, values}) => (
                            <View>
                                <Text>Old Password
                                    <Text style={{color: 'red'}}> (*)</Text>
                                </Text>
                                <Input
                                    onChangeText={handleChange('oldPassword')}
                                    onBlur={handleBlur('oldPassword')}
                                    value={values.oldPassword}
                                    secureTextEntry={true}
                                />
                                <View style={{marginBottom: 10}}>
                                    <Text style={{color: 'red'}}><ErrorMessage name="oldPassword"/></Text>
                                </View>
                                <Text>New Password
                                    <Text style={{color: 'red'}}> (*)</Text>
                                </Text>
                                <Input
                                    onChangeText={handleChange('newPassword')}
                                    onBlur={handleBlur('newPassword')}
                                    value={values.newPassword}
                                    secureTextEntry={true}
                                />
                                <View style={{marginBottom: 10}}>
                                    <Text style={{color: 'red'}}><ErrorMessage name="newPassword"/></Text>
                                </View>
                                <Button buttonStyle={{backgroundColor: "#ffa800"}} onPress={handleSubmit}
                                        title="Submit"/>
                                <View style={{marginTop: 10}}>
                                    {this.props.accountInfos.successStatus
                                        ?
                                        <Text style={{color: '#4caf50'}}>{this.props.accountInfos.successMessage}</Text>
                                        :
                                        this.props.accountInfos.errMess !== null && this.props.accountInfos.errMess.length > 0 ?
                                            this.mapErrMessToParagraph(this.props.accountInfos.errMess) :
                                            <Text></Text>}
                                </View>
                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});


const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
        modifyAccountInfo: async (
            userName,
            password,
            oldPassword,
            isChangePassword,
            roleId,
            departmentId,
            firstName,
            lastName,
            gender,
            dob,
            email,
            phoneNumber,
            cityProvinceId,
            districtId,
            wardCommuneId,
            address,
            disableFlag,
            screen_id
        ) => {
            dispatch(
                modifyAccountInfo(
                    userName,
                    password,
                    oldPassword,
                    isChangePassword,
                    roleId,
                    departmentId,
                    firstName,
                    lastName,
                    gender,
                    dob,
                    email,
                    phoneNumber,
                    cityProvinceId,
                    districtId,
                    wardCommuneId,
                    address,
                    disableFlag,
                    screen_id
                )
            );
        },
        clearAccountInfoErrMess: () => dispatch(clearAccountInfoErrMess()),
        postLogout: () => dispatch(postLogout()),
        clearOrders: () => dispatch(clearOrders())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ChangePassword
);
