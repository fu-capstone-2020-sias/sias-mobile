import React, { Component } from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import { ErrorMessage, Formik } from "formik";
import { Button, Input } from "react-native-elements";
import {
    clearAccountInfoErrMess,
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, fetchAccountInfo, modifyAccountInfo,
    postLogin, postLogout,
    postFeedback, clearFeedbackSuccess
} from "../../../redux/actions/ActionCreators";
import { connect } from "react-redux";
import { Loading } from "../../components/Loading";
import * as Yup from "yup";
import { BackHandler } from 'react-native';


const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const FeedbackSchema = Yup.object().shape({
    title: Yup.string()
        .required('Required.'),
    description: Yup.string()
        .required('Required.'),
});


class Feedback extends Component {

    componentDidMount() {
        this.props.clearFeedbackSuccess();
    }

    handleFeedback = async (values) => {
        await this.props.postFeedback(this.state.accountUserName, this.state.accountEmail, values.title, values.description);
    }

    state = {
        accountUserName: this.props.loginAccount.userName,
        accountEmail: this.props.loginAccount.email
    }
    render() {
        const errMess = this.props.feedbacks.errMess;
        const successStatus = this.props.feedbacks.successStatus;

        return (
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Send Feedback</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Formik
                        initialValues={
                            {
                                title: '',
                                description: ''
                            }
                        }
                        validationSchema={FeedbackSchema}
                        onSubmit={
                            values => this.handleFeedback(values)
                        }
                    >
                        {({ handleChange, handleBlur, handleSubmit, values }) => (
                            <View>
                                <Text>Title
                                    <Text style={{ color: 'red' }}> (*)</Text>
                                </Text>
                                <Input
                                    onChangeText={handleChange('title')}
                                    onBlur={handleBlur('title')}
                                    value={values.title}
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="title" /></Text>
                                </View>
                                <Text>Description
                                    <Text style={{ color: 'red' }}> (*)</Text>
                                </Text>
                                <Input
                                    onChangeText={handleChange('description')}
                                    onBlur={handleBlur('description')}
                                    value={values.description}
                                    multiline
                                />
                                <View style={{ marginBottom: 10 }}>
                                    <Text style={{ color: 'red' }}><ErrorMessage name="description" /></Text>
                                </View>
                                {
                                    successStatus == false &&
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ color: 'red' }}><Text>{errMess}</Text></Text>
                                    </View>
                                }
                                {
                                    successStatus == true &&
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ color: 'green' }}><Text>Your Feedback has been sent successfully</Text></Text>
                                    </View>
                                }
                                <Button buttonStyle={{ backgroundColor: "#ffa800" }} onPress={handleSubmit}
                                    title="Submit" />
                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});


const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
        feedbacks: state.feedbacks
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
        clearAccountInfoErrMess: () => dispatch(clearAccountInfoErrMess()),

        clearFeedbackSuccess: () => dispatch(clearFeedbackSuccess()),
        postFeedback: (accountUserName, accountEmail, title, description) => {
            dispatch(postFeedback(accountUserName, accountEmail, title, description))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    Feedback
);
