import React, {Component, useEffect, useState} from 'react';
import {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Platform,
    Keyboard, SafeAreaView, TouchableWithoutFeedback, Image, KeyboardAvoidingView, ScrollView
} from 'react-native';

import {Icon, Input, CheckBox, Button} from "react-native-elements";
// import CookieManager from '@react-native-community/cookies';

import {ErrorMessage, Formik} from 'formik';
import * as Yup from "yup";
import {connect} from 'react-redux';
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount,
    postLogin
} from "../../../redux/actions/ActionCreators";
import Home from "../home/Home";
import ForgotPassword from "./ForgotPassword";
import {createStackNavigator} from "react-navigation-stack";
import ViewAccount from "./ViewAccount";
import ChangePassword from "./ChangePassword";
import OrderManagement from "../order/OrderManagement";
import StockManagement from "../stock/StockManagement";
import Feedback from "./Feedback";
import {createAppContainer} from "react-navigation";
import {ROLE_FACTORY_MANAGER} from "../../../shared/constants";
import Switch from "react-native-paper";
import {Loading} from "../../components/Loading";

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
    };
};

const LoginSchema = Yup.object().shape({
    userName: Yup.string()
        .required('Required.'),
    password: Yup.string()
        .required('Required.'),
});


const sessionValidErrMess = (userName) => [
    `Hey there! You must log out your current account (${userName}) in order to login another account.`
]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const navigator = createStackNavigator(
    {
        Login: {
            screen: connect(mapStateToProps, mapDispatchToProps)(
                LoginScreen
            )
        },
        ForgotPassword: {
            screen: ForgotPassword
        },
    },
    {
        initialRouteName: "Login",
        defaultNavigationOptions:
            {
                headerShown: false
            }
    },
);


const Container = createAppContainer(navigator);


function LoginScreen(props) {

    useEffect(() => {
            console.log("check props");
            console.log(props);
            isValidSession(props);
        }
        , []);

    console.log("Check ERR MESS from HOME");
    console.log(props)

    // console.log("STATE login account");
    // console.log(props.loginAccount);
    // console.log(sessionValidErrMess(props.loginAccount.account.userName)[0])
    //
    // console.log("Check true false");
    // console.log(
    //     props.loginAccount.account !== null
    //     && (props.loginAccount.errMess === null ||
    //     (props.loginAccount.errMess[0]
    //         && props.loginAccount.errMess[0] === sessionValidErrMess(props.loginAccount.account.userName)[0]))
    // )

    // console.log("Check account");
    // console.log(props.loginAccount.account);

    if (props.loginAccount.account !== null) {

        console.log("CHAY DI VI CHUA");

        console.log(props.navigation);

        // return <Home navigation={props.navigation}/>
        // props.navigation.navigate("Home");

        props.navigation.goBack(null);
    }

    return (
        <ScrollView>
            <View style={styles.logoContainer}>
                <Image
                    style={styles.logo}
                    source={require('../../../assets/logo_sias.png')}>
                </Image>
            </View>

            <View style={styles.forContainer}>
                <Text>For Factory Manager</Text>
            </View>

            <View style={styles.formContainer}>
                <Formik
                    initialValues={
                        {
                            userName: '',
                            password: ''
                        }
                    }
                    validationSchema={LoginSchema}
                    onSubmit={
                        async values => {
                            console.log('POST LOGIN VALUES')
                            console.log(values)
                            await props.clearLoginCredentials();
                            await props.postLogin({
                                userName: values.userName,
                                password: values.password
                            })
                            console.log(props.loginAccount.account);
                        }
                    }
                >
                    {({handleChange, handleBlur, handleSubmit, values}) => (
                        <View>
                            <Text>Username
                                <Text style={{color: 'red'}}> (*)</Text>
                            </Text>
                            <Input
                                onChangeText={handleChange('userName')}
                                onBlur={handleBlur('userName')}
                                value={values.userName}
                            />
                            <View style={{marginBottom: 10}}>
                                <Text style={{color: 'red'}}><ErrorMessage name="userName"/></Text>
                            </View>

                            <Text>Password
                                <Text style={{color: 'red'}}> (*)</Text>
                            </Text>
                            <Input
                                secureTextEntry={true}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                            />
                            <View style={{marginBottom: 10}}>
                                <Text style={{color: "red"}}><ErrorMessage name="password"/></Text>
                            </View>
                            <Button buttonStyle={{backgroundColor: "#ffa800"}} onPress={handleSubmit}
                                    title="Login"/>
                        </View>
                    )}
                </Formik>

                <Text style={{marginTop: 10, color: 'red'}}>{props.loginAccount.errMess != null
                && props.loginAccount.errMess !== 'Missing credentials' ?
                    "1 - Your username or your password is incorrect.\n" +
                    "2 - You are not the Factory Manager." : ""}
                </Text>

                <TouchableOpacity onPress={() => {
                    props.navigation.navigate("ForgotPassword")
                }}>
                    <Text style={styles.forgotPassword}>Forgot Password?</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.copyright}>
                <Text>© 2020 Steel Industry Assistant System</Text>
            </View>
        </ScrollView>
    )
}

class LoginNavigateToForgotPassword extends Component {
    componentDidMount() {

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    paddingTop:
                        Platform.OS === "ios" ? 0 : Expo.Constants.statusBarHeight,
                }}
            >
                <Container/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 100,
        width: 100
    },
    logoContainer: {
        marginTop: 20,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    formContainer: {
        marginTop: 80,
        paddingLeft: 10,
        paddingRight: 10
    },
    forContainer: {
        marginTop: 10,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    copyright: {
        position: 'absolute',
        bottom: 0,
        left: 75
    },
    forgotPassword: {
        marginTop: 20,
        marginBottom: 160
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(
    LoginNavigateToForgotPassword
);
