import React, {Component} from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {Button, Input} from "react-native-elements";
import {
    clearAccountInfoErrMess,
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, clearResetPasswordSuccess, fetchAccountInfo, modifyAccountInfo,
    postLogin, postLogout, postResetPassword
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import Login from "./Login";
import {Loading} from "../../components/Loading";
import * as Yup from "yup";
import {BackHandler} from 'react-native';


const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

// const password = (val) => /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(val)


class ForgotPassword extends Component {

    componentDidMount() {
        this.props.clearResetPasswordSuccess();
    }

    handleResetPassword = async (values) => {
        await this.props.postResetPassword(values.userName);
    }

    state = {
        firstLoad: false,
        visibleOldPassword: false,
        visibleNewPassword: false
    };

    render() {


        const errMess = this.props.resetPasswordDetails.errMess;
        const email = this.props.resetPasswordDetails.email;

        return (
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Forgot Password</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Formik
                        initialValues={
                            {
                                userName: '',
                            }
                        }
                        onSubmit={
                            values => this.handleResetPassword(values)
                        }
                    >
                        {({handleChange, handleBlur, handleSubmit, values}) => (
                            <View>
                                <Text>Username
                                    <Text style={{color: 'red'}}> (*)</Text>
                                </Text>
                                <Input
                                    onChangeText={handleChange('userName')}
                                    onBlur={handleBlur('userName')}
                                    value={values.userName}
                                />
                                {
                                    errMess != null &&
                                    <View style={{marginBottom: 10}}>
                                        <Text style={{color: 'red'}}><Text>{errMess}</Text></Text>
                                    </View>
                                }
                                {
                                    email != null &&
                                    <View style={{marginBottom: 10}}>
                                        <Text style={{color: 'green'}}><Text>An email has been sent to your inbox
                                            ({email}).
                                            Please check it.</Text></Text>
                                    </View>
                                }
                                <Button buttonStyle={{backgroundColor: "#ffa800"}} onPress={handleSubmit}
                                        title="Reset Password"/>
                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});


const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
        resetPasswordDetails: state.resetPasswordDetails
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
        clearAccountInfoErrMess: () => dispatch(clearAccountInfoErrMess()),
        clearResetPasswordSuccess: () => dispatch(clearResetPasswordSuccess()),
        postResetPassword: (userName) => {
            dispatch(postResetPassword(userName))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ForgotPassword
);
