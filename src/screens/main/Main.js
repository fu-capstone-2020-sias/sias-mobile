import React, {Component} from "react";
import {
    View,
    Platform,
    ScrollView,
    Text,
    StyleSheet,
    Image,
    ToastAndroid,
} from "react-native";
import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer, SafeAreaView} from "react-navigation";
// import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";
import {Icon} from "react-native-elements";
import {connect} from "react-redux";

import ChangePassword from "../account/ChangePassword";
import Feedback from "../account/Feedback";
import Login from "../account/Login";
import ViewAccount from "../account/ViewAccount";

import Home from "../home/Home";

import StockManagement from "../stock/StockManagement";
import OrderManagement from "../order/OrderManagement";
import ImageMethod from "../stock/ImageMethod";

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => ({});

const navigator = createStackNavigator(
    {
        Login: Login,

        Home: Home,
        ViewAccount: ViewAccount,
        ChangePassword: ChangePassword,

        Order: OrderManagement,

        Stock: StockManagement,
        ImageMethod: ImageMethod,

        Feedback: Feedback,
    },
    {
        initialRouteName: "Home",
        defaultNavigationOptions:
            {
                headerShown: false
            }
    },
);


const Container = createAppContainer(navigator);

class Main extends Component {
    componentDidMount() {

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    paddingTop:
                        Platform.OS === "ios" ? 0 : Expo.Constants.statusBarHeight,
                }}
            >
                <Container/>
            </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
