import React, {Component} from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {Button, Input} from "react-native-elements";
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount, fetchAccountInfo, fetchOrderDetail, fetchOrderItems,
    postLogin
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import {Loading} from "../../components/Loading";
import {Card, DataTable} from 'react-native-paper';
import NumberFormat from 'react-number-format';

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

class OrderDetail extends Component {
    state = {
        account: null
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        await this.props.fetchOrderDetail(this.props.navigation.getParam("orderDetailId", ""));
        await this.props.fetchOrderItems(this.props.navigation.getParam("orderDetailId", ""))
    }

    mapOrderToRow = (orders) => {
        const ordersMapped = orders.map((orderObj, row) => {
            const zebraStyle = row % 2 !== 0 ? {backgroundColor: '#c8cbce'} : {}

            return (
                <DataTable.Row style={zebraStyle}>
                    <DataTable.Cell>{orderObj.orderItemId}</DataTable.Cell>

                    <DataTable.Cell>
                        <NumberFormat value={orderObj.length} displayType={'text'}
                                      thousandSeparator={true} renderText={value =>
                            <Text>{value}</Text>}/>
                    </DataTable.Cell>
                    <DataTable.Cell>{orderObj.bladeWidth}</DataTable.Cell>
                    <DataTable.Cell>
                        <NumberFormat value={orderObj.quantity} displayType={'text'}
                                      thousandSeparator={true} renderText={value =>
                            <Text>{value}</Text>}/>
                    </DataTable.Cell>
                    <DataTable.Cell>
                        <NumberFormat value={orderObj.price} displayType={'text'}
                                      thousandSeparator={true} renderText={value =>
                            <Text>{value}</Text>}/>
                    </DataTable.Cell>
                    <DataTable.Cell>{orderObj.createdDate ? orderObj.createdDate : "N/A"}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.creator ? orderObj.creator : "N/A"}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.updatedDate ? orderObj.updatedDate : "N/A"}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.updater ? orderObj.updater : "N/A"}</DataTable.Cell>
                </DataTable.Row>
            )
        })

        return ordersMapped;
    }

    mapStatusColor = (orderStatus) => {
        switch (orderStatus) {
            case 'Pending':
                return <Text style={{backgroundColor: '#6c757d'}}>{orderStatus}</Text>
            case 'Approved':
                return <Text style={{backgroundColor: '#17a2b8'}}>{orderStatus}</Text>
            case 'Processing':
                return <Text style={{backgroundColor: '#ffc107'}}>{orderStatus}</Text>
            case 'Completed':
                return <Text style={{backgroundColor: '#28a745'}}>{orderStatus}</Text>
            case 'Rejected':
                return <Text style={{backgroundColor: '#dc3545'}}>{orderStatus}</Text>
        }
    }


    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        console.log("Order Detail")
        console.log(this.props.orderDetail);

        let orderDetail;
        let orderItems;
        if (this.props.orderDetail !== null &&
            this.props.orderDetail !== undefined) {
            orderDetail = this.props.orderDetail.order;
            orderItems = this.props.orderDetail.items;
            if (orderDetail === undefined || orderDetail === null)
                return <Loading/>

            if (orderItems === undefined || orderItems === null)
                return <Loading/>

            // console.log("orderDetail");
            // console.log(orderDetail);
        } else
            return <Loading/>

        return (
            // Tạm thời sẽ thêm scrollview + nếu Accordion ok thì thêm vào cho đẹp và gọn hơn
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>View Order Detail</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Card>
                        <Card.Title title="Order Details"/>
                        <Card.Content>
                            {/* START: Customer Section */}
                            <ScrollView horizontal={true}>
                                <DataTable
                                    style={{width: 500}}
                                >
                                    <DataTable.Row>
                                        <DataTable.Cell>Customer Name:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.customerName}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Customer's Company:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.customerCompanyName}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Customer's Email:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.customerEmail}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Customer's Phone Number:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.customerPhoneNumber}</DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>Customer's Address:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.customerAddress + ", " + orderDetail.customerWardCommune + ", " + orderDetail.customerDistrict + ", " + orderDetail.customerCityProvince}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>------------------------------------------------------</DataTable.Cell>
                                        <DataTable.Cell></DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Order ID:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.orderId}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Status:</DataTable.Cell>
                                        <DataTable.Cell>{this.mapStatusColor(orderDetail.orderStatus)}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Total Price (VNĐ):</DataTable.Cell>
                                        <DataTable.Cell>
                                            <NumberFormat value={orderDetail.totalPrice} displayType={'text'}
                                                          thousandSeparator={true} renderText={value =>
                                                <Text>{value}</Text>}/>
                                        </DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Creator:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.creator}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Created Date:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.createdDate}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Updater:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.updater}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Updated Date:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.updatedDate}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Approver:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.approver}</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Assignee:</DataTable.Cell>
                                        <DataTable.Cell>{orderDetail.assignee}</DataTable.Cell>
                                    </DataTable.Row>
                                </DataTable>
                            </ScrollView>
                            {/* END: Customer Section */}
                        </Card.Content>
                    </Card>

                    <Card style={{marginTop: 20}}>
                        <Card.Title title="Order Items"/>
                        <Card.Content>
                            <ScrollView horizontal={true}>
                                <DataTable
                                    style={{width: 1400}}
                                >

                                    <DataTable.Header style={{backgroundColor: "#ffb74d"}}>
                                        <DataTable.Title>Item ID</DataTable.Title>
                                        <DataTable.Title>Length (mm)</DataTable.Title>
                                        <DataTable.Title>Blade Width (mm)</DataTable.Title>
                                        <DataTable.Title>Quantity</DataTable.Title>
                                        <DataTable.Title>Unit Price (VNĐ)</DataTable.Title>
                                        <DataTable.Title>Created Date</DataTable.Title>
                                        <DataTable.Title>Creator</DataTable.Title>
                                        <DataTable.Title>Updated Date</DataTable.Title>
                                        <DataTable.Title>Updater</DataTable.Title>
                                    </DataTable.Header>

                                    {this.mapOrderToRow(orderItems)}
                                </DataTable>
                            </ScrollView>
                        </Card.Content>
                    </Card>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});


const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        orderDetail: state.orderDetail,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchOrderDetail: (orderId) => {
            dispatch(fetchOrderDetail(orderId))
        },
        fetchOrderItems: (orderId) => {
            dispatch(fetchOrderItems(orderId))
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    OrderDetail
);
