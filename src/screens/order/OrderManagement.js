import React, {Component} from 'react';
import {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView, Platform
} from 'react-native';
import {Table, Row, Rows} from 'react-native-table-component';
import {createStackNavigator} from "react-navigation-stack";
import {connect} from "react-redux";
import ForgotPassword from "../account/ForgotPassword";
import {createAppContainer} from "react-navigation";
import {clearLoginCredentialsExceptAccount, fetchOrder, postLogin} from "../../../redux/actions/ActionCreators";
import OrderDetail from "./OrderDetail";
import Login from "../account/Login";
import {Loading} from "../../components/Loading";
import Button, {DataTable} from 'react-native-paper';
import {Searchbar} from 'react-native-paper';
import CuttingInstruction from "./CuttingInstruction";

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        exportOrderBatchs: state.exportOrderBatch,
        orders: state.orders,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchOrder: (pageNumber) => {
            dispatch(fetchOrder(pageNumber))
        },
        fetchOrderSearch: (pageNumber, searchData) => {
            dispatch(fetchOrder(pageNumber, 'POST', searchData))
        },
    };
};

class OrderManagement extends Component {

    state = {
        searchString: ""
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        const pageNumber = 1;

        await this.props.fetchOrder(pageNumber);
    }

    mapOrderToRow = (orders) => {
        const ordersMapped = orders.map((orderObj, row) => {
            const zebraStyle = row % 2 !== 0 ? {backgroundColor: '#c8cbce'} : {}

            return (
                <DataTable.Row style={zebraStyle}>
                    <DataTable.Cell>{orderObj.orderId}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.customerName}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.creator}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.createdDate}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.approver}</DataTable.Cell>
                    <DataTable.Cell>{orderObj.assignee}</DataTable.Cell>
                    <DataTable.Cell>
                        {this.mapStatusColor(orderObj.orderStatus)}
                    </DataTable.Cell>
                    <DataTable.Cell
                        onPress={() => this.props.navigation.navigate('OrderDetail', {orderDetailId: orderObj.orderId})}>
                        <Text style={{color: 'blue', textDecorationLine: 'underline'}}

                        >
                            Details
                        </Text>
                    </DataTable.Cell>
                    {orderObj.orderStatus === 'Processing' ?
                        <DataTable.Cell
                            onPress={() => this.props.navigation.navigate('CuttingInstruction', {
                                orderId: orderObj.orderId,
                                orderStatus: orderObj.orderStatus
                            })}>
                            <Text style={{color: 'blue', textDecorationLine: 'underline'}}
                            >
                                Cutting Instruction
                            </Text>
                        </DataTable.Cell> : <DataTable.Cell><Text></Text></DataTable.Cell>
                    }
                </DataTable.Row>
            )
        })

        return ordersMapped;
    }

    mapStatusColor = (orderStatus) => {
        switch (orderStatus) {
            case 'Pending':
                return <Text style={{backgroundColor: '#6c757d'}}>{orderStatus}</Text>
            case 'Approved':
                return <Text style={{backgroundColor: '#17a2b8'}}>{orderStatus}</Text>
            case 'Processing':
                return <Text style={{backgroundColor: '#ffc107'}}>{orderStatus}</Text>
            case 'Completed':
                return <Text style={{backgroundColor: '#28a745'}}>{orderStatus}</Text>
            case 'Rejected':
                return <Text style={{backgroundColor: '#dc3545'}}>{orderStatus}</Text>
        }
    }

    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        let orders;
        if (this.props.orders !== null &&
            this.props.orders !== undefined) {
            orders = this.props.orders.orders;
            if (orders === undefined || orders === null)
                return <Loading/>

            console.log("check orders");
            console.log(orders);
        } else
            return <Loading/>

        return (
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>View Order List</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Searchbar
                        placeholder="Search by Order ID"
                        onIconPress={async () => {
                            await this.props.fetchOrderSearch(1,
                                {
                                    orderId: this.state.searchString
                                })
                        }}
                        onSubmitEditing={async () => {
                            await this.props.fetchOrderSearch(1,
                                {
                                    orderId: this.state.searchString
                                })
                        }}
                        onChangeText={(query) => {
                            this.setState({
                                searchString: query
                            })
                        }}
                        value={this.state.searchString}
                    />
                </View>
                <View style={styles.formContainer}>
                    <ScrollView horizontal={true}>
                        <DataTable
                            style={{width: 1400}}
                        >

                            <DataTable.Header style={{backgroundColor: "#ffb74d"}}>
                                <DataTable.Title>ID</DataTable.Title>
                                <DataTable.Title>Customer</DataTable.Title>
                                <DataTable.Title>Creator</DataTable.Title>
                                <DataTable.Title>Created Date</DataTable.Title>
                                <DataTable.Title>Approver</DataTable.Title>
                                <DataTable.Title>Assignee</DataTable.Title>
                                <DataTable.Title>Status</DataTable.Title>
                                <DataTable.Title></DataTable.Title>
                                <DataTable.Title></DataTable.Title>
                            </DataTable.Header>

                            {this.mapOrderToRow(orders)}
                        </DataTable>
                    </ScrollView>
                    <Text style={{marginTop: 20}}>Total records: {this.props.orders.totalEntries}</Text>
                    <DataTable.Pagination
                        page={this.props.orders.currentPageNumber}
                        numberOfPages={this.props.orders.numberOfPages + 1}
                        onPageChange={async pageNumber => {
                            console.log("IS NANA")
                            console.log(pageNumber)
                            console.log(isNaN(pageNumber));

                            // const lastPage = Math.ceil(this.props.orders.totalEntries / 10);
                            //
                            // if (pageNumber === lastPage - 1) {
                            //     pageNumber
                            // }
                            if (pageNumber !== 0) {
                                if (this.state.searchString) {
                                    await this.props.fetchOrderSearch(pageNumber,
                                        {
                                            orderId: this.state.searchString
                                        })
                                    return;
                                }
                                await this.props.fetchOrder(pageNumber)
                            }
                        }}
                    />
                </View>
            </ScrollView>
        )
    }
}

const navigator = createStackNavigator(
    {
        OrderManagement: {
            screen: connect(mapStateToProps, mapDispatchToProps)(OrderManagement)
        },
        OrderDetail: {
            screen: OrderDetail
        },
        CuttingInstruction: {
            screen: CuttingInstruction
        },
    },
    {
        initialRouteName: "OrderManagement",
        defaultNavigationOptions:
            {
                headerShown: false
            }
    },
);


const Container = createAppContainer(navigator);

class OrderManagementNavigateToOrderDetail extends Component {
    componentDidMount() {

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    paddingTop:
                        Platform.OS === "ios" ? 0 : Expo.Constants.statusBarHeight,
                }}
            >
                <Container/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        marginTop: 0,
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderManagementNavigateToOrderDetail);
