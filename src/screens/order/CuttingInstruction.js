import React, {Component} from 'react';
import Header, {
    Text, StyleSheet, View, TextInput, TouchableOpacity,
    Keyboard, TouchableWithoutFeedback,
    SafeAreaView, ScrollView
} from 'react-native';
import {ErrorMessage, Formik} from "formik";
import {Button, Input} from "react-native-elements";
import {
    clearLoginCredentials,
    clearLoginCredentialsExceptAccount,
    fetchAccountInfo,
    fetchCuttingInstructionByOrderId,
    fetchOrderDetail,
    fetchOrderItems,
    postLogin
} from "../../../redux/actions/ActionCreators";
import {connect} from "react-redux";
import {Loading} from "../../components/Loading";
import {Card, DataTable} from 'react-native-paper';
import NumberFormat from 'react-number-format';

const sessionInvalidErrMess = () =>
    ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]

const missingCredentials = () =>
    ["Missing credentials"]

// Hàm để check Session còn valid trên server hay ko
export const isValidSession = async (props) => {
    props.clearLoginCredentialsExceptAccount();

    const postLoginAsync = async () => {
        await props.postLogin({
            userName: props.loginAccount.userName,
            password: 'checkAuthenAgain'
        })
    }

    postLoginAsync();
}

class CuttingInstruction extends Component {
    state = {
        account: null
    }

    async componentDidMount() {
        console.log("check props");
        console.log(this.props);
        await isValidSession(this.props);

        let orderId = this.props.navigation.getParam("orderId", null);
        await this.props.fetchCuttingInstructionByOrderId(orderId);
        await this.props.fetchOrderItems(orderId);
    }

    mapStatusColor = (orderStatus) => {
        switch (orderStatus) {
            case 'Pending':
                return <Text style={{backgroundColor: '#6c757d'}}>{orderStatus}</Text>
            case 'Approved':
                return <Text style={{backgroundColor: '#17a2b8'}}>{orderStatus}</Text>
            case 'Processing':
                return <Text style={{backgroundColor: '#ffc107'}}>{orderStatus}</Text>
            case 'Completed':
                return <Text style={{backgroundColor: '#28a745'}}>{orderStatus}</Text>
            case 'Rejected':
                return <Text style={{backgroundColor: '#dc3545'}}>{orderStatus}</Text>
        }
    }


    render() {
        if (this.props.loginAccount.account !== null) {
            if (this.props.loginAccount.errMess !== null) {
                if (this.props.loginAccount.errMess[0] === sessionInvalidErrMess()[0]) {
                    this.props.clearLoginCredentials();
                }

                if (this.props.loginAccount.errMess === missingCredentials()[0]) {
                    this.props.clearLoginCredentials();
                }
            }
        }

        if (this.props.loginAccount.account === null) {
            return <Login/>
        }

        let orderId = this.props.navigation.getParam("orderId", null);
        let orderStatus = this.props.navigation.getParam("orderStatus", null);

        let rows = null;
        if (this.props.fetchCuttingInstructions.cuttingInstruction !== null)
            rows = this.props.fetchCuttingInstructions.cuttingInstruction.cuttingInstruction;

        console.log("Check instruction");
        console.log(rows);

        let orderItems = this.props.orderDetail.items;

        console.log("Check orderItems");
        console.log(orderItems);

        if (rows === null) {
            if (orderItems !== null && orderItems.length >= 0) {
                return <ScrollView>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>View Cutting Instruction</Text>
                    </View>
                    {/* Border */}
                    <View
                        style={{
                            borderWidth: 1,
                            borderColor: '#ffc107',
                            margin: 10,
                        }}
                    />
                    <View style={styles.formContainer}>
                        <Text>There is no cutting instruction for this order (Order ID: {orderId}).</Text>
                        <Text style={{color: "red", marginTop: 20}}>Please use web-based system to create an
                            instruction!</Text>
                    </View>
                </ScrollView>
            }

            return <Loading/>
        }

        let checkExist = null;
        let skip = 0;

        return (
            // Tạm thời sẽ thêm scrollview + nếu Accordion ok thì thêm vào cho đẹp và gọn hơn
            <ScrollView>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>View Cutting Instruction</Text>
                </View>
                {/* Border */}
                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#ffc107',
                        margin: 10,
                    }}
                />
                <View style={styles.formContainer}>
                    <Text>Order ID: {orderId}</Text>
                    <Text>Order Status: {this.mapStatusColor(orderStatus)}</Text>
                    <ScrollView style={{marginTop: 20}} horizontal={true}>
                        <DataTable
                            style={{width: 1600}}
                        >
                            <DataTable.Header style={{backgroundColor: "#ffb74d"}}>
                                <DataTable.Title>Order Item ID</DataTable.Title>
                                <DataTable.Title>Cutting Instruction ID</DataTable.Title>
                                <DataTable.Title>Steel Bar #</DataTable.Title>
                                <DataTable.Title>Wanted Length (mm)</DataTable.Title>
                                <DataTable.Title>Blade Width (mm)</DataTable.Title>
                                <DataTable.Title>Stock Product ID to cut from</DataTable.Title>
                                <DataTable.Title>After-cut Stock Product Length (mm)</DataTable.Title>
                                <DataTable.Title>Assignee</DataTable.Title>
                            </DataTable.Header>

                            {rows.map((row) => {
                                    let orderItemId = row.orderItemId;
                                    skip = 0;
                                    if (checkExist === orderItemId) {
                                        skip = 1;
                                    }

                                    let orderItem = rows.map((row) => {
                                        if (row.orderItemId === orderItemId && skip === 0) {
                                            checkExist = orderItemId;
                                            return (
                                                <DataTable.Row>
                                                    <DataTable.Cell></DataTable.Cell>
                                                    <DataTable.Cell>
                                                        <NumberFormat value={row.cuttingInstructionId} displayType={'text'}
                                                                      thousandSeparator={true} renderText={value =>
                                                            <Text>{value}</Text>}/>
                                                    </DataTable.Cell>
                                                    <DataTable.Cell>
                                                        <NumberFormat value={row.steelBarNumber} displayType={'text'}
                                                                      thousandSeparator={true} renderText={value =>
                                                            <Text>{value}</Text>}/>
                                                    </DataTable.Cell>
                                                    <DataTable.Cell>
                                                        <NumberFormat value={row.steelLength} displayType={'text'}
                                                                      thousandSeparator={true} renderText={value =>
                                                            <Text>{value}</Text>}/>
                                                    </DataTable.Cell>
                                                    <DataTable.Cell>
                                                        <NumberFormat value={row.bladeWidth} displayType={'text'}
                                                                      thousandSeparator={true} renderText={value =>
                                                            <Text>{value}</Text>}/>
                                                    </DataTable.Cell>
                                                    <DataTable.Cell>
                                                        <NumberFormat value={row.stockProductId} displayType={'text'}
                                                                      thousandSeparator={true} renderText={value =>
                                                            <Text>{value}</Text>}/>
                                                    </DataTable.Cell>
                                                    <DataTable.Cell>
                                                        <NumberFormat value={row.stockProductLength} displayType={'text'}
                                                                      thousandSeparator={true} renderText={value =>
                                                            <Text>{value}</Text>}/>
                                                    </DataTable.Cell>
                                                    <DataTable.Cell>
                                                        {row.assignee}
                                                    </DataTable.Cell>
                                                </DataTable.Row>
                                            )
                                        }
                                        return null;
                                    })
                                    if (skip === 1) {
                                        return null;
                                    }
                                    return (
                                        <>
                                            <DataTable.Row style={{
                                                backgroundColor: "#c8cbce"
                                            }}>
                                                <DataTable.Cell> {row.orderItemId}</DataTable.Cell>
                                            </DataTable.Row>
                                            {orderItem}
                                        </>
                                    )
                                }
                            )}
                        </DataTable>
                    </ScrollView>
                    {/*<Text style={{marginTop: 20}}>Total records: {this.props.orders.totalEntries}</Text>*/}
                    {/*<DataTable.Pagination*/}
                    {/*    page={this.props.orders.currentPageNumber}*/}
                    {/*    numberOfPages={this.props.orders.numberOfPages + 1}*/}
                    {/*    onPageChange={async pageNumber => {*/}
                    {/*        console.log("IS NANA")*/}
                    {/*        console.log(pageNumber)*/}
                    {/*        console.log(isNaN(pageNumber));*/}

                    {/*        // const lastPage = Math.ceil(this.props.orders.totalEntries / 10);*/}
                    {/*        //*/}
                    {/*        // if (pageNumber === lastPage - 1) {*/}
                    {/*        //     pageNumber*/}
                    {/*        // }*/}
                    {/*        if (pageNumber !== 0)*/}
                    {/*            await this.props.fetchOrder(pageNumber)*/}
                    {/*    }}*/}
                    {/*/>*/}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    titleContainer: {
        paddingLeft: 10,
        paddingRight: 10
    },
    formContainer: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
});


const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        fetchCuttingInstructions: state.fetchCuttingInstruction,
        orderDetail: state.orderDetail,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        postLogin: async (
            loginDetails
        ) => {
            dispatch(postLogin(loginDetails));
        },
        clearLoginCredentials: () => {
            dispatch(clearLoginCredentials());
        }
        ,
        clearLoginCredentialsExceptAccount: () => {
            dispatch(clearLoginCredentialsExceptAccount());
        },
        fetchCuttingInstructionByOrderId: (orderId) => {
            dispatch(fetchCuttingInstructionByOrderId(orderId))
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    CuttingInstruction
);
