import React from 'react';
import { Text, StyleSheet, View, TextInput, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Feather } from '@expo/vector-icons';

const SearchBar = () => {
    return (
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss}}>
            <View style={styles.background}>
                <Feather name="search" size={30} />
                <TextInput style={styles.inputStyle} placeholder="Search"/>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    background: {
        backgroundColor: 'white',
        height: 30,
        borderRadius: 10,
        marginHorizontal: 10,
        flexDirection: "row",
    },
    inputStyle: {
        flex: 1,
    }
});

export default SearchBar; 