import React from "react";
import {ActivityIndicator, StyleSheet, Text, View} from "react-native";

const styles = StyleSheet.create({
    loadingView: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },
    loadingText: {
        color: "#0000ff",
        fontSize: 18,
        fontWeight: "bold",
    },
});

export const Loading = (props) => {
    return (
        <View style={styles.loadingView}>
            <ActivityIndicator size="large" color="#0000ff"/>
            <Text style={styles.loadingText}> {!props.message ? "Loading . . ." : props.message} </Text>
        </View>
    );
};

