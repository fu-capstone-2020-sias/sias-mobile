import React from "react";
import Main from "./src/screens/main/Main";
import { Provider } from "react-redux";
import { ConfigureStore } from "./redux/store/configureStore";
import { PersistGate } from "redux-persist/es/integration/react";
import { Loading } from "./src/components/Loading";

// const  store  = ConfigureStore();
//
// // export default function App() {
// //     return (
// //         <Provider store={store}>
// //                 <Main />
// //         </Provider>
// //     );
// // }

const { persistor, store } = ConfigureStore();

import { YellowBox } from "react-native";
YellowBox.ignoreWarnings([""]);

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={<Loading />} persistor={persistor}>
        <Main />
      </PersistGate>
    </Provider>
  );
}
