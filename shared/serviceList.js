export const serviceList = [
    {
        id: 1,
        name: 'Warehouse',
        description: 'Description',
        details: [
            {
                name: 'View Warehouse List',
                description: "View a list of your company's warehouse",
                url: '#'
            },
            {
                name: 'Add a new Warehouse',
                description: 'Add a new warehouse to the database',
                url: '#'
            }
        ]
    },
    {
        id: 2,
        name: 'Stock Products',
        description: 'Description',
        details: [
            {
                name: 'View Stock Product List',
                description: "View a list of your company's stock products",
                url: '/viewstockproductlist'
            },
            {
                name: 'Add new Stock Products',
                description: 'Add new stock product(s) to the database',
                url: '/addstockproduct'
            }
        ]
    },
    {
        id: 3,
        name: 'Orders',
        description: 'Description',
        details: [
            {
                name: 'View Order List',
                description: "View a list of orders from customers",
                url: '/vieworderlist'
            },
            {
                name: 'Add new Orders',
                description: "Add new order(s) with customers' information",
                url: '/addorder'
            }
        ]
    },
    {
        id: 4,
        name: 'Company',
        description: 'Description',
        details: [
            {
                name: 'View Company Information',
                description: 'Get to know your company',
                url: '/viewCompany'
            },
            {
                name: 'Feedback',
                description: "Send feedback to company's administrators",
                url: '/feedback'
            }
        ]
    }
];