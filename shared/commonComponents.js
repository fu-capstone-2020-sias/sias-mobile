import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import { Link } from 'react-router-dom';

export const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.warning.light,
        color: theme.palette.common.black,
        fontSize: 15,
        fontWeight: "bold",
    },
    body: {
        fontSize: 14,
        borderRight: "1px solid #e0e0e0"
    },
}))(TableCell);

export function EnhancedTableHead(props) {
    const headCells = props.headCells;
    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    );
};

export function TablePaging(props) {
    const numberOfPages = props.numberOfPages;
    const linkTo = props.linkTo;
    if (numberOfPages < 1) return (<div></div>);
    else if (numberOfPages < 10) {
        const pageArray = [...Array(numberOfPages).keys()].map(i => i + 1);
        const currentPage = props.currentPage;
        const allPages = pageArray.map(i => {
            return (
                <span key={i}>&nbsp;
                    {(i === currentPage) ? <Link key={i} className="link active" to='#'>{i}</Link> :
                        <Link key={i} className="link" to={`${linkTo}/${i}`}>{i}</Link>}
                &nbsp;</span>
            );
        });
        return (
            <div align="right" className="table-paging">
                <h5>Pages: {allPages} </h5>
            </div>
        )
    } else {
        const currentPage = props.currentPage;
        const first3Pages = [1, 2, 3].map(i => {
            return (
                <span key={i}>&nbsp;
                    {(i === currentPage) ? <Link className="link active" to='#'>{i}</Link> :
                        <Link key={i} className="link" to={`${linkTo}/${i}`}>{i}</Link>}
                &nbsp;</span>
            );
        });
        const last3Pages = [numberOfPages - 2, numberOfPages - 1, numberOfPages].map(i => {
            return (
                <span>&nbsp;
                    {(i === currentPage) ? <Link className="link active" to='#'>{i}</Link> :
                        <Link key={i} className="link" to={`${linkTo}/${i}`}>{i}</Link>}
                &nbsp;</span>
            );
        });
        const isCurrentPageIncluded = !(currentPage < 4 || currentPage > numberOfPages - 3);
        const currentPageIfIncluded = (
            <span>&nbsp;
                {<Link key={currentPage} className="link active" to='#'>{currentPage}</Link>}
            &nbsp;</span>
        )
        return (
            <div align="right" className="table-paging">
                <h5>Pages:{first3Pages}...{isCurrentPageIncluded ? { currentPageIfIncluded } : ''}{last3Pages}</h5>
            </div>
        );
    }
};

export const statusDisplay = (status) => {
    let className = 'p-2 mb-2 ';
    switch (status) {
        case 'Approved':
            className += 'bg-info text-white';
            break;
        case 'Pending':
            className += 'bg-secondary text-white';
            break;
        case 'Processing':
            className += 'bg-warning';
            break;
        case 'Completed':
            className += 'bg-success text-white';
            break;
        case 'Rejected':
            className += 'bg-danger';
            break;
        default:
            className += '';
            break;
    }
    return <span className={className}>{status}</span>
}

export const disableFlagDisplay = (disableFlag) => {
    let className = 'p-2 mb-2 ';
    let text = 'Active';
    switch (disableFlag) {        
        case 1:
            className += 'bg-danger';
            text = 'Disabled';
            break;
        default:
            className += '';
            break;
    }
    return <span className={className}>{text}</span>
}