export const required = (val) => val && val.length;
export const requiredNumber = (val) => val !== '0';
export const maxLength = (len) => (val) => (!val) || (val.length <= len);
export const minLength = (len) => (val) => val && (val.length >= len);
export const exactLength = (len) => (val) => val && (val.length === len);

export const validCustomerName = (val) => /^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z ]+$/i.test(val);
export const validCompanyName = (val) => /^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]+$/.test(val);
export const validWarehouseName = (val) => /^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]*$/.test(val);
export const validUserName = (val) => /^[A-Za-z0-9]{0,20}$/i.test(val);
export const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);
export const validPhoneNumber = (val) => /^(03|07|08|09|01[2|6|8|9])+([0-9]{8})$/i.test(val);
export const validNumberField = (val, fromLen, toLen) => new RegExp(`^[0-9]{${fromLen},${toLen}}$`).test(val);
// export const validNumberField = (val, fromLen, toLen) => new RegExp(`^[0-9]{${fromLen},${toLen}}$`).test(val);
export const validBarCode = (val) => /^[0-9]*$/i.test(val);
export const validPrice = (val) => /^[0-9]{1,10}$/i.test(val);
export const validLength = (val) => /^[0-9]{1,5}$/i.test(val);
export const validSKU = (val) => /^[A-Za-z0-9-]*$/i.test(val);
export const validProductName = (val) => /^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]*$/.test(val);