import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from 'react-redux-form';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { AccountInfos } from "../reducers/accountInfos";
import { AddNewAccounts } from "../reducers/addNewAccounts";
import { DeleteDisabledAccount } from "../reducers/deleteDisabledAccount";
import { ImportOrderBatch } from "../reducers/importOrderBatch";
import { FetchCuttingInstructions } from "../reducers/fetchCuttingInstruction";
import { CreateCuttingInstruction } from "../reducers/createInstruction";
import { ExportOrderBatch } from "../reducers/exportOrderBatch";
import { ImportStockProductBatch } from "../reducers/importStockProductBatch";
import {
    InitialAccountInfo,
    InitialCompanyInfo,
    InitialFeedback,
    InitialNewAccount,
    InitialOrderDetail,
    InitialStockProductDetail,
    InitialWarehouseDetail,
} from '../reducers/forms';
import { LoginAccount } from '../reducers/loginAccount';
import { Feedbacks } from '../reducers/feedbacks';
import { Orders } from '../reducers/orders';
import { Accounts } from '../reducers/accounts';
import { StockProducts } from '../reducers/stockProducts'
import { OrderDetail, CurrentlyViewedOrder } from '../reducers/orderDetail';
import { CompanyInfos } from '../reducers/companyInfos'
import { ResetPasswordDetails } from '../reducers/resetPasswordDetails';
import { Warehouses } from '../reducers/wareHouses'
import { Addresses } from '../reducers/addresses';
import { SteelInfos } from '../reducers/steelInfos';
import { StockProductDetail, CurrentlyViewedStockProduct } from '../reducers/stockProductDetail';
import { ExportStockBatch } from "../reducers/exportStockBatch";


export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            loginAccount: LoginAccount,
            feedbacks: Feedbacks,
            orders: Orders,
            stockProducts: StockProducts,
            steelInfos: SteelInfos,
            accounts: Accounts,
            resetPasswordDetails: ResetPasswordDetails,
            currentlyViewedOrder: CurrentlyViewedOrder,
            orderDetail: OrderDetail,
            currentlyViewedStockProduct: CurrentlyViewedStockProduct,
            stockProductDetail: StockProductDetail,
            companyInfos: CompanyInfos,
            accountInfos: AccountInfos,
            addNewAccounts: AddNewAccounts,
            deleteDisabledAccounts: DeleteDisabledAccount,
            importOrderBatch: ImportOrderBatch,
            fetchCuttingInstruction: FetchCuttingInstructions,
            createCuttingInstruction: CreateCuttingInstruction,
            exportOrderBatch: ExportOrderBatch,
            exportStockBatch: ExportStockBatch,
            importStockProductBatch: ImportStockProductBatch,
            warehouses: Warehouses,
            addresses: Addresses,
            ...createForms({
                feedback: InitialFeedback,
                companyInfo: InitialCompanyInfo,
                accountInfo: InitialAccountInfo,
                addNewAccount: InitialNewAccount,
                editOrderDetail: InitialOrderDetail,
                editStockProductDetail: InitialStockProductDetail,
                addOrder: InitialOrderDetail,
                addNewStockProduct: InitialStockProductDetail,
                addWarehouse: InitialWarehouseDetail,
            })
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
};