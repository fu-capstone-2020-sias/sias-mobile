import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
// import { dishes } from "./dishes";
// import { comments } from "./comments";
// import { promotions } from "./promotions";
// import { leaders } from "./leaders";
// import { favorites } from "./favorites";
import {persistStore, persistCombineReducers} from "redux-persist";
import storage from "redux-persist/es/storage";
// import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-community/async-storage';
import {LoginAccount} from "../reducers/loginAccount";
import {AccountInfos} from "../reducers/accountInfos";
import {ResetPasswordDetails} from "../reducers/resetPasswordDetails";
import {Orders} from "../reducers/orders";
import {OrderDetail} from "../reducers/orderDetail";
import {FetchCuttingInstructions} from "../reducers/fetchCuttingInstruction";
import {StockProducts} from "../reducers/stockProducts";
import {StockProductDetail} from "../reducers/stockProductDetail";
import {SteelInfos} from "../reducers/steelInfos";
import {Feedbacks} from "../reducers/feedbacks";
import {ImageUpload} from "../reducers/imageUpload";
import {Warehouses, WarehouseSelections} from '../reducers/wareHouses'

export const ConfigureStore = () => {
    const config = {
        key: "root",
        storage: AsyncStorage,
        debug: true,
    };

    const store = createStore(
        persistCombineReducers(config, {
            loginAccount: LoginAccount,
            accountInfos: AccountInfos,
            resetPasswordDetails: ResetPasswordDetails,
            orders: Orders,
            orderDetail: OrderDetail,
            fetchCuttingInstruction: FetchCuttingInstructions,
            stocks: StockProducts, 
            stockDetail: StockProductDetail, 
            steelInfos: SteelInfos,
            feedbacks: Feedbacks,
            imageUpload: ImageUpload,
            warehouseSelections: WarehouseSelections,
        }),
        applyMiddleware(thunk, logger)
    );

    const persistor = persistStore(store);

    return {persistor, store};
};

// export const ConfigureStore = () => {
//     const store = createStore(
//         combineReducers({
//             loginAccount: LoginAccount,
//         }),
//         applyMiddleware(thunk, logger)
//     );
//
//     return store;
// };
