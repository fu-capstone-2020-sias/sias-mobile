import * as ActionTypes from './ActionTypes';
import {baseUrl} from '../../shared/baseUrl';
import qs from 'qs';
// import Cookies from 'js-cookie';
import AsyncStorage from '@react-native-community/async-storage';
import {
    ORDER_STATUS_PENDING,
    ERROR_MESSAGE_SUBMIT_GENERAL,
    ERROR_MESSAGE_UPLOAD_IMAGE_GENERAL
} from '../../shared/constants';

/*////////////////////
SUBMIT ERRORS (FOR ALL POST ACTIONS)
////////////////////*/

export const submitFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.SUBMIT_FAILED,
        payload: errMess
    });
}

/*////////////////////
MARK ORDERS AS DONE ACTIONS
////////////////////*/

export const markDoneOrder = (res) => {
    //returning an action
    return ({
        type: ActionTypes.MARK_DONE_ORDER,
        payload: res
    });
}

export const postMarkDoneOrder = (arrOfOrderIds) => (dispatch) => {
    const newRequest = {
        arrOfOrderIds: arrOfOrderIds
    }

    const data = qs.stringify(newRequest);
    alert(data);

    return fetch(baseUrl + 'order/markDone', {
        method: 'POST',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(markDoneOrder(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
POST FEEDBACK ACTIONS
////////////////////*/

export const clearFeedbackSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_FEEDBACK_SUCCESS,
        payload: null
    });
}

export const clearFeedback = () => (dispatch) => {
    dispatch(clearFeedbackSuccess());
}

export const addFeedback = (res) => {
    //returning an action
    return ({
        type: ActionTypes.ADD_FEEDBACK,
        payload: res
    });
}

export const postFeedback = (senderName, senderEmail, title, description) => (dispatch) => {
    const newFeedback = {
        title: title,
        description: description,
        senderName: senderName,
        senderEmail: senderEmail
    }

    const data = qs.stringify(newFeedback);
    // alert(data);

    return fetch(baseUrl + 'feedback', {
        method: 'POST',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => dispatch(addFeedback(response)))
        .catch(error => {
            console.log('Post feedback error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
FETCH FEEDBACK ACTIONS
////////////////////*/

export const fetchFeedback = (pageNumber) => (dispatch) => {
    return fetch(baseUrl + 'feedback?page=' + pageNumber, {
        method: 'GET',
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            // console.log("this is response");
            // console.log(response);
            response.currentPageNumber = pageNumber;
            dispatch(fetchFeedbackSuccessfully(response))
        })
        .catch(error => dispatch(fetchFeedbackFailed(error.message)));
}

export const fetchFeedbackSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_FEEDBACKS,
        payload: res
    });
}

export const fetchFeedbackFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.FEEDBACKS_FAILED,
        payload: errMess
    });
}


/*////////////////////
ADD WAREHOUSE
////////////////////*/

export const addWarehouse = (res) => {
    //returning an action
    return ({
        type: ActionTypes.ADD_WAREHOUSE,
        payload: res
    });
}

export const postNewWarehouse = (values) => (dispatch) => {
    const newWarehouse = {
        name: values.warehouseName,
        cityProvinceId: values.cityProvinceId,
        districtId: values.districtId,
        wardCommuneId: values.communeId,
        address: values.address,
        parentDepartmentId: values.department,
        headOfWarehouse: values.warehouseManager,
    }

    const data = qs.stringify(newWarehouse);
    // alert(data);

    return fetch(baseUrl + 'warehouse', {
        method: 'POST',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => dispatch(addWarehouse(response)))
        .catch(error => {
            console.log('Post new order error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
FETCH WAREHOUSE MANAGER ACTIONS
////////////////////*/

export const fetchWarehouseManagerIdList = () => (dispatch) => {
    return fetch(baseUrl + "warehouseManager", {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            const a = 1
            dispatch(fetchWarehouseManagerIdListSuccessfully(res.data.rows));
        })
        .catch(error => dispatch(fetchAddressFailed(error.message)));
}

export const fetchWarehouseManagerIdListSuccessfully = (res) => {
    //returning an action
    console.log("manager=");
    console.log(res);
    return ({
        type: ActionTypes.FETCH_WAREHOUSE_MANAGER,
        payload: res
    });
}

/*////////////////////
FETCH DEPARTMENT ACTIONS
////////////////////*/

export const fetchDepartmentIdList = () => (dispatch) => {
    return fetch(baseUrl + "department", {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchDepartmentIdListSuccessfully(res.data.rows));
        })
        .catch(error => dispatch(fetchAddressFailed(error.message)));
}

export const fetchDepartmentIdListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_DEPARTMENT,
        payload: res
    });
}

/*////////////////////
FETCH WAREHOUSE LIST ACTIONS
////////////////////*/

export const fetchWarehouse = (pageNumber, searchData = null) => (dispatch) => {
    const requestUrl = 'warehouse?pageNumber=' + pageNumber;
    const params = (searchData == null) ? '' : '&' + qs.stringify(searchData);
    // alert(params);

    let requestObject = {
        method: 'GET',
        credentials: 'include'
    }

    return fetch(baseUrl + requestUrl + params, requestObject)
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            console.log("this is response");
            console.log(response);
            response.data.currentPageNumber = pageNumber;

            dispatch(fetchWarehouseProductSuccessfully(response.data))

        })
        .catch(error => dispatch(fetchWarehouseProductFailed(error.message)));
}

export const fetchWarehouseProductSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_WAREHOUSE,
        payload: res
    });
}

export const fetchWarehouseProductFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.WAREHOUSE_FAILED,
        payload: errMess
    });
}

/*////////////////////
DELETE WARE HOUSE ACTIONS
////////////////////*/

export const warehousesDeleted = (res) => {
    //returning an action
    return ({
        type: ActionTypes.DELETE_WAREHOUSE
    });
}

export const deleteWarehouse = (warehouseId) => (dispatch) => {
    const newRequest = {
        warehouseId: warehouseId
    }

    const data = qs.stringify(newRequest);
    // alert(data);

    return fetch(baseUrl + 'warehouse', {
        method: 'DELETE',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(warehousesDeleted(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
FETCH STOCK PRODUCT LIST ACTIONS
////////////////////*/

export const fetchStockProduct = (pageNumber, searchData = null) => (dispatch) => {
    const requestUrl = 'stockProduct?pageNumber=' + pageNumber;
    console.log("SEARCH DATA");
    console.log(searchData);
    const params = (searchData == null) ? '' : '&' + 'stockProductId=' + searchData.stockProductId;
    // alert(params);

    console.log("PARAMMMMMMMMMMMMMMMMMMMMMMM");
    console.log(params);

    let requestObject = {
        method: 'GET',
        credentials: 'include'
    }

    return fetch(baseUrl + requestUrl + params, requestObject)
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            console.log("this is response");
            console.log(response);
            response.data.currentPageNumber = pageNumber;
            if (searchData != null) {
                response.data.searchData = searchData;
                dispatch(fetchStockProductSearch(response.data))
            } else {
                dispatch(fetchStockProductSuccessfully(response.data))
            }
        })
        .catch(error => dispatch(fetchStockProductFailed(error.message)));
}

export const fetchStockProductSearch = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_STOCK_PRODUCTS_SEARCH,
        payload: res
    });
}

export const fetchStockProductSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_STOCK_PRODUCTS,
        payload: res
    });
}

export const fetchStockProductFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.STOCK_PRODUCTS_FAILED,
        payload: errMess
    });
}

/*////////////////////
FETCH STOCK PRODUCT DETAIL ACTIONS
////////////////////*/

export const fetchStockProductDetail = (stockProductId) => async (dispatch) => {
    // dispatch({
    //     type: ActionTypes.ORDER_DETAIL_LOADING
    // });

    return fetch(baseUrl + 'stockProduct/' + stockProductId, {
        method: 'GET',
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            // console.log("this is response");
            // console.log(response);
            dispatch(fetchStockProductDetailSuccessfully(response));
        })
        .catch(error => dispatch(fetchStockProductDetailFailed(error.message)));
}

export const fetchStockProductDetailSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_STOCK_PRODUCT_DETAIL,
        payload: res.data.rows[0]
    });
}

export const fetchStockProductDetailFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.STOCK_PRODUCT_DETAIL_FAILED,
        payload: errMess
    });
}

/*////////////////////
MODIFY/DELETE STOCK PRODUCT ACTIONS
////////////////////*/

export const stockProductsDeleted = (res) => {
    //returning an action
    return ({
        type: ActionTypes.DELETE_STOCK_PRODUCTS
    });
}

export const deleteStockProducts = (stockProductId) => (dispatch) => {
    const newRequest = {
        stockProductId: stockProductId
    }

    const data = qs.stringify(newRequest);
    // alert(data);

    return fetch(baseUrl + 'stockProduct', {
        method: 'DELETE',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(stockProductsDeleted(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

export const modifyStockProductDetail = (res) => {
    //returning an action
    return ({
        type: ActionTypes.MODIFY_STOCK_PRODUCT_DETAIL,
        payload: res
    });
}

export const putModifyStockProductDetail = (values) => (dispatch) => {
    const newRequest = {
        stockProductId: values.stockProductId,
        sku: values.sku,
        barcode: values.barcode,
        type: values.type,
        name: values.name,
        price: values.price,
        length: values.length,
        stockGroupId: values.stockGroupId,
        standard: values.standard,
        statusId: values.statusId,
        manufacturedDate: new Date(values.manufacturedYear, values.manufacturedMonth, values.manufacturedDay),
        warehouseId: values.warehouseId,
    }
    alert(JSON.stringify(newRequest));

    const data = qs.stringify(newRequest);
    // alert(data);

    return fetch(baseUrl + 'stockProduct', {
        method: 'PUT',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(modifyStockProductDetail(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
ADD NEW STOCK PRODUCT ACTIONS
////////////////////*/

export const clearStockProductDetailSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_STOCK_PRODUCT_DETAIL,
        payload: null
    })
}

export const clearStockProductDetail = () => (dispatch) => {
    dispatch(clearStockProductDetailSuccess());
}

export const addStockProduct = (res) => {
    //returning an action
    return ({
        type: ActionTypes.ADD_STOCK_PRODUCT,
        payload: res.insertedStockProduct
    });
}

export const postNewStockProduct = (values) => (dispatch) => {
    const newStockProduct = {
        statusId: values.statusId,
        sku: values.sku,
        barcode: values.barcode,
        type: values.type,
        name: values.name,
        price: values.price,
        length: values.length,
        stockGroupId: values.stockGroupId,
        standard: values.standard,
        warehouseId: values.warehouseId,
        manufacturedDate: values.manufacturedDate
    }

    const data = qs.stringify(newStockProduct);
    // alert(data);

    return fetch(baseUrl + 'stockProduct', {
        method: 'POST',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => dispatch(addStockProduct(response)))
        .catch(error => {
            console.log('Post new stockProduct error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}


/*////////////////////
FETCH ORDER LIST ACTIONS
////////////////////*/

export const fetchOrder = (pageNumber, method = 'GET', searchData = null) => (dispatch) => {
    const requestUrl = (method === 'GET') ? 'order?page=' : 'order/search?page=';
    const data = (searchData == null) ? {} : qs.stringify(searchData);
    // alert(data);

    let requestObject = {
        method: method,
        credentials: 'include'
    }

    if (method === 'POST') {
        requestObject.body = data;
        requestObject.headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
    }

    return fetch(baseUrl + requestUrl + pageNumber, requestObject)
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            // console.log("this is response");
            // console.log(response);
            response.currentPageNumber = pageNumber;
            if (method === 'POST') {
                response.searchData = searchData;
                dispatch(fetchOrderSearch(response))
            } else {
                dispatch(fetchOrderSuccessfully(response))
            }
        })
        .catch(error => dispatch(fetchOrderFailed(error.message)));
}

export const fetchOrderSearch = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_ORDERS_SEARCH,
        payload: res
    });
}

export const fetchOrderSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_ORDERS,
        payload: res
    });
}

export const fetchOrderFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.ORDERS_FAILED,
        payload: errMess
    });
}

export const ordersDeleted = (res) => {
    //returning an action
    return ({
        type: ActionTypes.DELETE_ORDERS
    });
}

export const deleteOrders = (arrOfOrderIds) => (dispatch) => {
    const newRequest = {
        arrOfOrderIds: arrOfOrderIds
    }

    const data = qs.stringify(newRequest);
    // alert(data);

    return fetch(baseUrl + 'order', {
        method: 'DELETE',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(ordersDeleted(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
FETCH ORDER DETAIL ACTIONS
////////////////////*/

export const fetchOrderDetail = (orderId) => async (dispatch) => {
    // dispatch({
    //     type: ActionTypes.ORDER_DETAIL_LOADING
    // });

    return fetch(baseUrl + 'order/' + orderId, {
        method: 'GET',
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            // console.log("this is response");
            // console.log(response);
            dispatch(fetchOrderDetailSuccessfully(response));
        })
        .catch(error => dispatch(fetchOrderDetailFailed(error.message)));
}

export const fetchOrderDetailSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_ORDER_DETAIL,
        payload: res.order
    });
}

export const fetchOrderDetailFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.ORDER_DETAIL_FAILED,
        payload: errMess
    });
}

/*////////////////////
FETCH ORDER ITEMS ACTIONS
////////////////////*/

export const fetchOrderItems = (orderId) => async (dispatch) => {
    return fetch(baseUrl + 'order/item?orderId=' + orderId, {
        method: 'GET',
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            // console.log("this is response");
            // console.log(response);
            dispatch(fetchOrderItemsSuccessfully(response));
        })
        .catch(error => dispatch(fetchOrderItemsFailed(error.message)));
}

export const fetchOrderItemsSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_ORDER_ITEMS,
        payload: res.orderItems
    });
}

export const fetchOrderItemsFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.ORDER_ITEMS_FAILED,
        payload: errMess
    });
}

/*////////////////////
MODIFY/ADD/DELETE ORDER ITEMS ACTIONS
////////////////////*/

export const submitModifyItemsFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.SUBMIT_MODIFY_ITEMS_FAILED,
        payload: errMess
    });
}

export const modifyOrderItems = (res) => {
    //returning an action
    return ({
        type: ActionTypes.MODIFY_ORDER_ITEMS,
        payload: res
    });
}

export const putModifyOrderItems = (orderId, modifiedItems) => (dispatch) => {
    const newRequestObject = modifiedItems.map(row => ({
        orderItemId: row.orderItemId,
        length: row.length,
        bladeWidth: row.bladeWidth,
        quantity: row.quantity,
        price: row.price
    }));
    const newRequestBody = JSON.stringify(newRequestObject)
    // alert(newRequestBody);

    return fetch(baseUrl + `order/${orderId}/item`, {
        method: 'PUT',
        body: newRequestBody,
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(modifyOrderItems(response));
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitModifyItemsFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

export const addOrderItems = (res) => {
    //returning an action
    return ({
        type: ActionTypes.ADD_ORDER_ITEMS,
        payload: res
    });
}

export const postAddOrderItems = (orderId, addedItems) => (dispatch) => {
    const newRequestObject = addedItems.map(row => ({
        length: row.length,
        bladeWidth: row.bladeWidth,
        quantity: row.quantity,
        price: row.price,
        updater: null,
        updatedDate: null
    }));
    const newRequestBody = JSON.stringify(newRequestObject)
    // alert(newRequestBody);

    return fetch(baseUrl + `order/${orderId}/item`, {
        method: 'POST',
        body: newRequestBody,
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(addOrderItems(response));
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitModifyItemsFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

export const deleteOrderItems = (res) => {
    //returning an action
    return ({
        type: ActionTypes.DELETE_ORDER_ITEMS,
        payload: res
    });
}

export const delDeleteOrderItems = (deletedItems) => (dispatch) => {
    const arrOfOrderItemIds = deletedItems.map(row => row.orderItemId);
    const newRequest = {
        arrOfOrderItemIds: arrOfOrderItemIds.toString()
    }
    const data = qs.stringify(newRequest);
    // alert(data);

    return fetch(baseUrl + `order/items`, {
        method: 'DELETE',
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(deleteOrderItems(response));
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitModifyItemsFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
MODIFY/EDIT ORDER DETAIL ACTIONS
////////////////////*/

export const modifyOrderDetail = (res) => {
    //returning an action
    return ({
        type: ActionTypes.MODIFY_ORDER_DETAIL,
        payload: res
    });
}

export const putModifyOrderDetail = (values, screenId) => (dispatch) => {
    let customerCityProvinceId = values.customerCityProvinceId;
    let customerDistrictId = values.customerDistrictId;
    let customerWardCommuneId = values.customerWardCommuneId;
    let customerAddress = values.customerAddress;
    if (values.editAddress) {
        customerCityProvinceId = values.newCustomerCityProvinceId;
        customerDistrictId = values.newCustomerDistrictId;
        customerWardCommuneId = values.newCustomerWardCommuneId;
        customerAddress = values.newCustomerAddress;
    }
    const newRequest = {
        orderId: values.orderId,
        orderStatusId: values.orderStatusId,
        customerName: values.customerName,
        customerCompanyName: values.customerCompanyName,

        customerCityProvinceId: customerCityProvinceId,
        customerDistrictId: customerDistrictId,
        customerWardCommuneId: customerWardCommuneId,
        customerAddress: customerAddress,

        customerPhoneNumber: values.customerPhoneNumber,
        customerEmail: values.customerEmail,
        totalPrice: values.totalPrice,
        creator: values.creator,
        approver: values.approver,
        assignee: values.assignee,
        screen_id: screenId
    }
    alert(JSON.stringify(newRequest));

    const data = qs.stringify(newRequest);
    // alert(data);

    return fetch(baseUrl + 'order', {
        method: 'PUT',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(modifyOrderDetail(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

export const putModifyOrderStatus = (orderId, orderStatusId) => (dispatch) => {
    const newRequest = {
        orderId: orderId,
        orderStatusId: orderStatusId
    }
    // alert(JSON.stringify(newRequest));

    const data = qs.stringify(newRequest);
    alert(baseUrl + 'order/changeOrderStatus?' + data);

    return fetch(baseUrl + 'order/changeOrderStatus?' + data, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            dispatch(modifyOrderDetail(response));
            window.location.reload(false);
        })
        .catch(error => {
            console.log('Action error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
ADD NEW ORDER ACTIONS (edited)
////////////////////*/

export const addOrder = (res) => {
    //returning an action
    return ({
        type: ActionTypes.ADD_ORDER,
        payload: res
    });
}

export const postNewOrder = (values) => (dispatch) => {
    const newOrder = {
        orderStatusId: ORDER_STATUS_PENDING,
        customerName: values.customerName,
        customerCompanyName: values.customerCompanyName,
        customerCityProvinceId: values.customerCityProvinceId,
        customerDistrictId: values.customerDistrictId,
        customerWardCommuneId: values.customerWardCommuneId,
        customerAddress: values.customerAddress,
        customerPhoneNumber: values.customerPhoneNumber,
        customerEmail: values.customerEmail,
        totalPrice: values.totalPrice,
        approver: values.approver,
        assignee: values.assignee
    }

    const data = qs.stringify(newOrder);
    // alert(data);

    return fetch(baseUrl + 'order', {
        method: 'POST',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                if (response.ok || response.status === 400) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            if (response.newOrder != null) {
                return dispatch(addOrder(response));
            } else if (response.error != null) {
                return dispatch(submitFailed(response.error));
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        })
        .catch(error => {
            console.log('Post new order error: ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });
}

/*////////////////////
FETCH ACCOUNT LIST ACTIONS
////////////////////*/

export const fetchAccount = (pageNumber, method = 'GET', searchData = null) => (dispatch) => {
    const requestUrl = (method === 'GET') ? 'account?page=' : 'account/search?page=';
    const data = (searchData == null) ? {} : qs.stringify(searchData);
    // alert(data);

    let requestObject = {
        method: method,
        credentials: 'include'
    }

    if (method === 'POST') {
        requestObject.body = data;
        requestObject.headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
    }

    return fetch(baseUrl + requestUrl + pageNumber, requestObject)
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => response.json())
        .then(response => {
            console.log("this is response");
            console.log(response);
            response.currentPageNumber = pageNumber;
            if (method === 'POST') {
                response.searchData = searchData;
                dispatch(fetchAccountSearch(response))
            } else {
                dispatch(fetchAccountSuccessfully(response))
            }
        })
        .catch(error => dispatch(fetchAccountFailed(error.message)));
}

export const fetchAccountSearch = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_ACCOUNTS_SEARCH,
        payload: res
    });
}

export const fetchAccountSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_ACCOUNTS,
        payload: res
    });
}

export const fetchAccountFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.ACCOUNTS_FAILED,
        payload: errMess
    });
}

/*////////////////////
LOGIN ACTIONS
////////////////////*/

export const loginSuccessfully = (account) => {
    return ({
        type: ActionTypes.LOGIN_SUCCESSFULLY,
        payload: account
    });
}

export const loginFailed = (errMess) => {
    return ({
        type: ActionTypes.LOGIN_FAILED,
        payload: errMess
    });
}

export const loginFormOpen = () => {
    // console.log('here');
    return ({
        type: ActionTypes.LOGIN_FORM_OPEN
    });
}

export const openLoginForm = () => (dispatch) => {
    dispatch(loginFormOpen());
}

export const clearLoginCredentialsExceptAccountSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_LOGIN_CREDENTIALS_EXCEPT_ACCOUNT_SUCCESS,
    })
}

export const clearLoginCredentialsExceptAccount = () => async (dispatch) => {
    dispatch(clearLoginCredentialsExceptAccountSuccess());
}

export const clearLoginCredentialsSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_LOGIN_CREDENTIALS_SUCCESS,
    })
}

export const clearLoginCredentials = () => async (dispatch) => {
    dispatch(clearLoginCredentialsSuccess());
}


export const postLogin = (loginDetails) => async (dispatch) => {

    await dispatch(clearLoginCredentialsExceptAccount());
    // await dispatch(clearLoginCredentials());

    const data = qs.stringify(loginDetails);
    // alert(data);

    return await fetch(baseUrl + 'auth/login', {
        method: 'POST',
        body: data,
        // mode: "cors",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'include'
    })
        .then(response => {
                // if (response.ok || response.status === 400) {
                return response
                //     } else {
                //         var error = new Error('Error ' + response.status + ': ' + response.statusText);
                //         error.response = response;
                //         throw error;
                //     }
                // },
                // error => {
                //     var errMess = new Error(error.message);
                //     throw errMess;
                // }
            }
        )
        .then(response => {
            return response.json();
        })
        .then(async response => {
            console.log("This is response");
            console.log(response);
            if (response.account == null) {

                if (response.message) {
                    dispatch(loginFailed(response.message));
                    return;
                }

                dispatch(loginFailed(response.error))
            } else {
                // async storage cookies here
                async function call() {
                    await AsyncStorage.setItem("account", JSON.stringify(response.account));
                }

                await call();

                dispatch(loginSuccessfully(response.account));
            }

        })
        .catch(error => {
            console.log('Login ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });

}

/*////////////////////
RESET PASSWORD ACTIONS
////////////////////*/
export const clearResetPasswordSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_RESET_PASSWORD_SUCCESS,
        payload: null
    });
}

export const clearResetPassword = () => (dispatch) => {
    dispatch(clearResetPasswordSuccess());
}


export const resetPasswordSuccessfully = (email) => {
    return ({
        type: ActionTypes.RESET_PASSWORD_SUCCESSFULLY,
        payload: email
    });
}

export const resetPasswordFailed = (errMess) => {
    return ({
        type: ActionTypes.RESET_PASSWORD_FAILED,
        payload: errMess
    });
}

export const postResetPassword = (userName) => (dispatch) => {

    return fetch(baseUrl + 'auth/forgotPassword/' + userName, {
        method: 'POST',
        // body: data,
        // mode: "cors",
        // headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded'
        // },
        credentials: 'include'
    })
        .then(response => {
                return response;
                // if (response.ok || response.status === 401) {
                //     return response;
                // }
                // } else {
                //     var error = new Error('Error ' + response.status + ': ' + response.statusText);
                //     error.response = response;
                //     throw error;
                // }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => {
            return response.json();
        })
        .then(response => {
            // console.log("This is response");
            // console.log(response);
            console.log("This is response");
            console.log(response);

            if (response.error !== null && response.error !== undefined) {
                if (response.error.length > 0) {
                    dispatch(resetPasswordFailed(response.error))
                    return;
                }
            }

            if (response.email !== null && response.email !== undefined) {
                dispatch(resetPasswordSuccessfully(response.email));
                return;
            }


        })
        .catch(error => {
            console.log('Reset password ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });

}

/*////////////////////
LOGOUT ACTIONS
////////////////////*/


export const logoutSuccessfully = (message) => {
    return ({
        type: ActionTypes.LOGOUT_SUCCESSFULLY,
        payload: message
    });
}

export const postLogout = () => (dispatch) => {

    return fetch(baseUrl + 'auth/logout', {
        method: 'POST',
        // mode: "cors",
        credentials: 'include'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(response => {
            return response.json();
        })
        .then(response => {
            dispatch(clearLoginCredentials());
            dispatch(logoutSuccessfully(response.message));
        })
        .catch(error => {
            console.log('Logout ', error.message);
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        });

}


/*////////////////////
COMPANY INFORMATION ACTIONS
////////////////////*/

export const fetchCompanyInfoSuccess = (resp) => {
    console.log("Check fetchCompanyInfoSuccess");
    console.log(resp);
    return {
        type: ActionTypes.FETCH_COMPANY_INFO,
        payload: resp,
    };
};

export const fetchCompanyInfoFailed = (errMess) => {
    return {
        type: ActionTypes.COMPANY_INFO_FAILED,
        payload: errMess,
    };
};

// No need to pass ID because server get Company Info ID of user on server
export const fetchCompanyInfo = () => (dispatch) => {
    return fetch(baseUrl + "company", {
        method: "GET",
        credentials: "include",
    })
        .then(
            (response) => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error(
                        "Error " + response.status + ": " + response.statusText
                    );
                    error.response = response;
                    throw error;
                }
            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => response.json())
        .then((response) => {
            if (response !== undefined && response.error !== undefined) {
                dispatch(fetchCompanyInfoFailed(response.error));
                return;
            }
            dispatch(fetchCompanyInfoSuccess(response));
        })
        .catch((err) => {
            dispatch(fetchCompanyInfoFailed(err.message));
        });
};

export const modifyCompanyInfoFailed = (messageArr) => {
    return {
        type: ActionTypes.MODIFY_COMPANY_INFO_FAILED,
        payload: messageArr, // Array
    };
};

export const modifyCompanyInfoSuccess = (message) => {
    return {
        type: ActionTypes.MODIFY_COMPANY_INFO_SUCCESS,
        payload: message,
    };
};

export const modifyCompanyInfo = (
    name,
    cityProvinceId,
    districtId,
    wardCommuneId,
    address,
    phoneNumber,
    email,
    notes,
    companyWebsite
) => async (dispatch) => {
    let newCompanyInfo = {
        name: name,
        cityProvinceId: cityProvinceId,
        districtId: districtId,
        wardCommuneId: wardCommuneId,
        address: address,
        phoneNumber: phoneNumber,
        email: email,
        notes: notes,
        companyWebsite: companyWebsite,
    };
    const data = qs.stringify(newCompanyInfo);
    return await fetch(baseUrl + "company", {
        method: "PUT",
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        credentials: "include",
    })
        .then(
            (response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    var error = new Error(
                        "Error " + response.status + ": " + response.statusText
                    );
                    error.response = response;
                    throw error;
                }
            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(modifyCompanyInfoFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(modifyCompanyInfoSuccess(response.message));
        })
        .catch((error) => {
            console.log("Put company info error: ", error.message);
            alert("Your request could not be sent\nError: " + error.message);
        });
};

/*////////////////////
ACCOUNT INFORMATION/PROFILE ACTIONS
////////////////////*/
export const fetchAccountInfoSuccess = (resp) => {
    console.log("Check fetchAccountInfoSuccess");
    console.log(resp);
    return {
        type: ActionTypes.FETCH_ACCOUNT_INFO,
        payload: resp.account,
    };
};

export const fetchAccountInfoFailed = (errMess) => {
    return {
        type: ActionTypes.ACCOUNT_INFO_FAILED,
        payload: errMess,
    };
};

// No need to pass ID because server get Company Info ID of user on server
export const fetchAccountInfo = (userName) => async (dispatch) => {
    return await fetch(baseUrl + "account/" + userName, {
        method: "GET",
        credentials: "include",
    })
        .then(
            (response) => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error(
                        "Error " + response.status + ": " + response.statusText
                    );
                    error.response = response;
                    throw error;
                }
            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => response.json())
        .then((response) => {
            if (response.error !== undefined && response.error.length > 0) {
                dispatch(fetchAccountInfoFailed(response.error));
                return;
            }
            dispatch(fetchAccountInfoSuccess(response));
        })
        .catch((err) => {
            dispatch(fetchAccountInfoFailed(err.message));
        });
};

export const clearAccountInfoErrMessSuccess = () => {
    return {
        type: ActionTypes.CLEAR_ACCOUNT_INFO_ERRMESS,
        payload: null, // Array
    };
};

export const clearAccountInfoErrMess = () => (dispatch) => {
    dispatch(clearAccountInfoErrMessSuccess());
}

export const modifyAccountInfoFailed = (messageArr) => {
    return {
        type: ActionTypes.MODIFY_ACCOUNT_INFO_FAILED,
        payload: messageArr, // Array
    };
};

export const modifyAccountInfoSuccess = (message) => {
    return {
        type: ActionTypes.MODIFY_ACCOUNT_INFO_SUCCESS,
        payload: message,
    };
};

export const modifyAccountInfo = (
    userName,
    password,
    oldPassword,
    isChangePassword,
    roleId,
    departmentId,
    firstName,
    lastName,
    gender,
    dob,
    email,
    phoneNumber,
    cityProvinceId,
    districtId,
    wardCommuneId,
    address,
    disableFlag,
    screen_id
) => async (dispatch) => {
    let accountInfo = {
        userName,
        password: password,
        oldPassword: oldPassword,
        isChangePassword: isChangePassword,
        roleId: roleId,
        departmentId: departmentId,
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        dob: dob,
        email: email,
        phoneNumber: phoneNumber,
        cityProvinceId: cityProvinceId,
        districtId: districtId,
        wardCommuneId: wardCommuneId,
        address: address,
        disableFlag: disableFlag,
        screen_id: screen_id
    };
    const data = qs.stringify(accountInfo);
    console.log("Check wardCommuneId");
    return await fetch(baseUrl + "account", {
        method: "PUT",
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        credentials: "include",
    })
        .then(
            (response) => {

                return response.json();

            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(modifyAccountInfoFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(modifyAccountInfoSuccess(response.message));
        })
        .catch((error) => {
            console.log("Put account info error: ", error.message);
            alert("Your request could not be sent\nError: " + error.message);
        });
};

/*////////////////////
ADD NEW ACCOUNT
////////////////////*/
export const addNewAccountFailed = (messageArr) => {
    return {
        type: ActionTypes.ADD_NEW_ACCOUNT_FAILED,
        payload: messageArr, // Array
    };
};

export const addNewAccountSuccess = (message) => {
    return {
        type: ActionTypes.ADD_NEW_ACCOUNT_SUCCESS,
        payload: message,
    };
};

export const addNewAccount = (
    userName,
    password,
    isChangePassword,
    roleId,
    departmentId,
    firstName,
    lastName,
    gender,
    dob,
    email,
    phoneNumber,
    cityProvinceId,
    districtId,
    wardCommuneId,
    address,
    disableFlag,
    screen_id
) => async (dispatch) => {
    let newAccount = {
        userName,
        password: password,
        isChangePassword: isChangePassword,
        roleId: roleId,
        departmentId: departmentId,
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        dob: dob,
        email: email,
        phoneNumber: phoneNumber,
        cityProvinceId: cityProvinceId,
        districtId: districtId,
        wardCommuneId: wardCommuneId,
        address: address,
        disableFlag: disableFlag,
        screen_id: screen_id
    };
    const data = qs.stringify(newAccount);
    alert(data);
    console.log("Check wardCommuneId");
    return await fetch(baseUrl + "account", {
        method: "POST",
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        credentials: "include",
    })
        .then(
            (response) => {

                return response.json();

            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(addNewAccountFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(addNewAccountSuccess(response.message));
        })
        .catch((error) => {
            console.log("POST new account error: ", error.message);
            alert("Your request could not be sent\nError: " + error.message);
        });
};

/*////////////////////
DELETE DISABLED ACCOUNT
////////////////////*/
export const deleteDisabledAccountFailed = (messageArr) => {
    return {
        type: ActionTypes.DELETE_DISABLED_ACCOUNT_FAILED,
        payload: messageArr, // Array
    };
};

export const deleteDisabledAccountSuccess = (message) => {
    return {
        type: ActionTypes.DELETE_DISABLED_ACCOUNT_SUCCESS,
        payload: message,
    };
};

export const deleteDisabledAccount = (
    arrOfUsernames
) => async (dispatch) => {
    let disabledAccount = {
        arrOfUsernames: arrOfUsernames
    };
    const data = qs.stringify(disabledAccount);
    alert(data);
    return await fetch(baseUrl + "account", {
        method: "DELETE",
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        credentials: "include",
    })
        .then(
            (response) => {

                return response.json();

            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(deleteDisabledAccountFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(deleteDisabledAccountSuccess(response.message));
        })
        .catch((error) => {
            console.log("DELETE disabled account error: ", error.message);
            alert("Your request could not be sent\nError: " + error.message);
        });
};

/*////////////////////
IMPORT ORDER BATCH
////////////////////*/
export const importOrderBatchSuccess = (messageArr) => {
    return {
        type: ActionTypes.IMPORT_ORDER_BATCH_SUCCESS,
        payload: messageArr, // Array
    };
};

export const importOrderBatchFailed = (message) => {
    return {
        type: ActionTypes.IMPORT_ORDER_BATCH_FAILED,
        payload: message,
    };
};

export const importOrderBatch = (
    formData
) => async (dispatch) => {
    // const data = qs.stringify(csvFile);
    // alert(data);

    console.log("CSV FILE");
    console.log(formData);
    return await fetch(baseUrl + "order/orderCSV", {
        method: "POST",
        body: formData,
        // headers: {
        //     "Content-Type": "multipart/form-data",
        // },
        credentials: "include",
    })
        .then(
            (response) => {

                return response.json();

            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(importOrderBatchFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(importOrderBatchSuccess(response.message));
        })
        .catch((error) => {
            console.log("POST Order CSV error: ", error.message);
            alert("Your request could not be sent\nError: " + error.message);
        });
};

export const clearImportOrderBatchSuccess = () => {
    return {
        type: ActionTypes.CLEAR_IMPORT_ORDER_BATCH_SUCCESS,
        payload: "",
    };
}

export const clearImportOrderBatch = () => (dispatch) => {
    dispatch(clearImportOrderBatchSuccess());
}

/*////////////////////
FETCH CUTTING INSTRUCTION
////////////////////*/
export const fetchCuttingInstructionByOrderIdSuccess = (resp) => {
    console.log(resp);
    return {
        type: ActionTypes.FETCH_CUTTING_INSTRUCTION_SUCCESS,
        payload: resp,
    };
};

export const fetchCuttingInstructionByOrderIdFailed = (errMess) => {
    return {
        type: ActionTypes.FETCH_CUTTING_INSTRUCTION_FAILED,
        payload: errMess,
    };
};

// No need to pass ID because server get Company Info ID of user on server
export const fetchCuttingInstructionByOrderId = (orderId) => async (dispatch) => {
    return await fetch(baseUrl + "order/cuttingInstruction?orderId=" + orderId, {
        method: "GET",
        credentials: "include",
    })
        .then(
            (response) => {
                return response.json();
            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(fetchCuttingInstructionByOrderIdFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(fetchCuttingInstructionByOrderIdSuccess(response));
        })
        .catch((err) => {
            dispatch(fetchCuttingInstructionByOrderIdFailed(err.message));
        });
};

/*////////////////////
FETCH CUTTING INSTRUCTION
////////////////////*/
export const createInstructionSuccess = (resp) => {
    console.log(resp);
    return {
        type: ActionTypes.CREATE_INSTRUCTION_SUCCESS,
        payload: resp,
    };
};

export const createInstructionFailed = (errMess) => {
    return {
        type: ActionTypes.CREATE_INSTRUCTION_FAILED,
        payload: errMess,
    };
};

// No need to pass ID because server get Company Info ID of user on server
export const createInstruction = (orderId) => async (dispatch) => {
    return await fetch(baseUrl + "order/createInstruction?orderId=" + orderId, {
        method: "POST",
        credentials: "include",
    })
        .then(
            (response) => {
                return response.json();
            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error.length > 0) {
                    dispatch(createInstructionFailed(response.error)); // Array
                    return;
                }
            }
            dispatch(createInstructionSuccess(response.message));
        })
        .catch((err) => {
            dispatch(createInstructionFailed(err.message));
        });
};

/*////////////////////
EXPORT ORDER TO CSV
////////////////////*/
export const exportOrderBatchSuccess = (messageArr) => {
    return {
        type: ActionTypes.EXPORT_ORDER_BATCH_SUCCESS,
        payload: messageArr, // Array
    };
};

export const exportOrderBatchFailed = (message) => {
    return {
        type: ActionTypes.EXPORT_ORDER_BATCH_FAILED,
        payload: message,
    };
};

//
// export const exportOrderBatch = (
//     arrOfOrderIds // string
// ) => async (dispatch) => {
//     let dataSend = {
//         arrOfOrderIds: arrOfOrderIds
//     };
//     alert(arrOfOrderIds.toString());
//     const data = qs.stringify(dataSend);
//     // alert(data);
//     return await fetch(baseUrl + "order/exportOrderCSV", {
//         method: "POST",
//         body: data,
//         headers: {
//             "Content-Type": "application/x-www-form-urlencoded",
//         },
//         credentials: "include",
//     })
//         .then(
//             (response) => {
//
//                 return response.blob()
//
//                 // return response.json();
//
//             },
//             (error) => {
//                 var errMess = new Error(error.message);
//                 throw errMess;
//             }
//         ).then((blob) => {
//             FileSaver.saveAs(blob, "exported_orders.csv");
//         }).catch((err) => {
//             dispatch(exportOrderBatchFailed(err.message));
//         });
// };

export const clearExportOrderBatchSuccess = () => {
    return {
        type: ActionTypes.CLEAR_EXPORT_ORDER_BATCH_SUCCESS,
        payload: "",
    };
}

export const clearExportOrderBatch = () => (dispatch) => {
    dispatch(clearExportOrderBatchSuccess());
}

/*////////////////////
IMPORT STOCK PRODUCT BATCH
////////////////////*/
export const importStockProductBatchSuccess = (messageArr) => {
    return {
        type: ActionTypes.IMPORT_STOCK_PRODUCT_BATCH_SUCCESS,
        payload: messageArr, // Array
    };
};

export const importStockProductBatchFailed = (message) => {
    return {
        type: ActionTypes.IMPORT_STOCK_PRODUCT_BATCH_FAILED,
        payload: message,
    };
};

export const importStockProductBatch = (
    formData
) => async (dispatch) => {
    // const data = qs.stringify(csvFile);
    // alert(data);

    console.log("CSV FILE");
    console.log(formData);
    return await fetch(baseUrl + "stockProduct/import", {
        method: "POST",
        body: formData,
        // headers: {
        //     "Content-Type": "multipart/form-data",
        // },
        credentials: "include",
    })
        .then(
            (response) => {

                return response.json();

            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.errorList !== undefined && response.errorList.length > 0) {
                    dispatch(importStockProductBatchFailed(response.errorList)); // Array
                    return;
                }
            }
            dispatch(importStockProductBatchSuccess(response.message));
        })
        .catch((error) => {
            console.log("POST Order CSV error: ", error.message);
            alert("Your request could not be sent\nError: " + error.message);
        });
};

export const clearStockProductBatchSuccess = () => {
    return {
        type: ActionTypes.CLEAR_IMPORT_STOCK_PRODUCT_BATCH_SUCCESS,
        payload: "",
    };
}

export const clearStockProductBatch = () => (dispatch) => {
    dispatch(clearStockProductBatchSuccess());
}
/*////////////////////
FIRST TIME LOADING FETCH FUNCTIONS
////////////////////*/

export const fetchCityProvinceIdList = () => (dispatch) => {
    return fetch(baseUrl + "cityProvince", {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchCityProvinceListSuccessfully(res.data.rows));
        })
        .catch(error => dispatch(fetchAddressFailed(error.message)));
}

export const fetchCityProvinceListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_CITY_PROVINCE,
        payload: res
    });
}

export const fetchDistrictIdList = (cityProvinceId = null) => (dispatch) => {
    const requestUrl = (cityProvinceId == null) ? "district" : "district/" + cityProvinceId;
    return fetch(baseUrl + requestUrl, {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchDistrictListSuccessfully(res.data.rows));
        })
        .catch(error => dispatch(fetchAddressFailed(error.message)));
}

export const fetchDistrictListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_DISTRICT,
        payload: res
    });
}

export const fetchWardCommuneIdList = (districtId = null) => (dispatch) => {
    const requestUrl = (districtId == null) ? "wardCommune" : "wardCommune/" + districtId;
    return fetch(baseUrl + requestUrl, {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchWardCommuneListSuccessfully(res.data.rows));
        })
        .catch(error => dispatch(fetchAddressFailed(error.message)));
}

export const fetchWardCommuneListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_WARD_COMMUNE,
        payload: res
    });
}

export const fetchAddressFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.ADDRESS_FAILED,
        payload: errMess
    });
}


export const fetchSteelTypeList = () => (dispatch) => {
    return fetch(baseUrl + "steelType", {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchSteelTypeListSuccessfully(res.data.data.rows));
        })
        .catch(error => dispatch(fetchSteelInfoFailed(error.message)));
}

export const fetchSteelTypeListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_STEEL_TYPE,
        payload: res
    });
}

export const fetchStockGroupList = () => (dispatch) => {
    return fetch(baseUrl + "stockGroup", {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchStockGroupListSuccessfully(res.data.data.rows));
        })
        .catch(error => dispatch(fetchSteelInfoFailed(error.message)));
}

export const fetchStockGroupListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_STOCK_GROUP,
        payload: res
    });
}

export const fetchStockProductStandardList = () => (dispatch) => {
    return fetch(baseUrl + "stockProductStandard", {
        method: "GET",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchStockProductStandardListSuccessfully(res.data.data.rows));
        })
        .catch(error => dispatch(fetchSteelInfoFailed(error.message)));
}

export const fetchStockProductStandardListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_STOCK_PRODUCT_STANDARD,
        payload: res
    });
}

export const fetchSteelInfoFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.STEEL_INFO_FAILED,
        payload: errMess
    });
}

/*////////////////////
EXPORT ORDER TO CSV
////////////////////*/
export const exportStockBatchSuccess = (messageArr) => {
    return {
        type: ActionTypes.EXPORT_STOCK_BATCH_SUCCESS,
        payload: messageArr, // Array
    };
};

export const exportStockBatchFailed = (message) => {
    return {
        type: ActionTypes.EXPORT_STOCK_BATCH_FAILED,
        payload: message,
    };
};

//
// export const exportStockBatch = (
//     idList // string
// ) => async (dispatch) => {
//     let dataSend = {
//         idList: idList
//     };
//     alert(idList.toString());
//     const data = qs.stringify(dataSend);
//     // alert(data);
//     return await fetch(baseUrl + "stockProduct/export", {
//         method: "POST",
//         body: data,
//         headers: {
//             "Content-Type": "application/x-www-form-urlencoded",
//         },
//         credentials: "include",
//     })
//         .then(
//             (response) => {
//
//                 return response.blob()
//
//                 // return response.json();
//
//             },
//             (error) => {
//                 var errMess = new Error(error.message);
//                 throw errMess;
//             }
//         ).then((blob) => {
//             FileSaver.saveAs(blob, "exported_stocks.csv");
//         }).catch((err) => {
//             dispatch(exportStockBatchFailed(err.message));
//         });
// };

export const clearExportStockBatchSuccess = () => {
    return {
        type: ActionTypes.CLEAR_EXPORT_STOCK_BATCH_SUCCESS,
        payload: "",
    };
}

export const clearExportStockBatch = () => (dispatch) => {
    dispatch(clearExportStockBatchSuccess());
}


export const clearOrdersSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_ORDER_SUCCESS,
    })
}

export const clearOrders = () => async (dispatch) => {
    dispatch(clearOrdersSuccess());
}

//LinhNT 


/*////////////////////
ADD STOCK PRODUCT BY UPLOADING IMAGE (OCR)
////////////////////*/

export const uploadImageSuccess = (dataToInsert) => {
    return {
        type: ActionTypes.UPLOAD_IMAGE_SUCCESSFULLY,
        payload: dataToInsert
    };
};

export const uploadImageFailed = (message) => {
    return {
        type: ActionTypes.UPLOAD_IMAGE_FAILED,
        payload: message
    };
};

export const uploadImage = (formData) => async (dispatch) => {
    console.log("IMAGE FILE");
    console.log(formData);

    return await fetch(baseUrl + "stockProduct/uploadImage", {
        method: "POST",
        body: formData,
        credentials: "include",
    })
        .then(
            (response) => {
                return response.json();
            },
            (error) => {
                var errMess = new Error(error.message);
                throw errMess;
            }
        )
        .then((response) => {
            console.log("RESSSSSSPONSEEEEEEEE");
            console.log(response);
            if (response !== undefined || response !== null) {
                if (response.error !== undefined && response.error != null) {
                    console.log("POST Image error: ", response.error);
                    dispatch(uploadImageFailed(ERROR_MESSAGE_UPLOAD_IMAGE_GENERAL));
                    return;
                } else if (response.dataToInsert !== undefined && response.dataToInsert != null) {
                    dispatch(uploadImageSuccess(response.dataToInsert));
                    return;
                }
            }
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
        })
        .catch((error) => {
            dispatch(submitFailed(ERROR_MESSAGE_SUBMIT_GENERAL));
            console.log("POST Image error: ", error.message);
        });
}

export const imageUploading = () => {
    return {
        type: ActionTypes.UPLOADING_IMAGE
    };
}

export const uploadingImage = () => (dispatch) => {
    dispatch(imageUploading());
}

export const clearImageUploadSuccess = () => {
    return ({
        type: ActionTypes.CLEAR_IMAGE_UPLOAD,
        payload: null
    });
}

export const clearImageUpload = () => (dispatch) => {
    dispatch(clearImageUploadSuccess());
}

/*////////////////////
FETCH WAREHOUSE SELECTION LIST
////////////////////*/

export const fetchWarehouseSelectionList = () => (dispatch) => {
    return fetch(baseUrl + "warehouse/all", {
        method: "POST",
        credentials: "include",
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errMess = new Error(error.message);
                throw errMess;
            })
        .then(res => res.json())
        .then(res => {
            dispatch(fetchWarehouseSelectionListSuccessfully(res.data));
        })
        .catch(error => dispatch(fetchWarehouseSelectionListFailed(error.message)));
}

export const fetchWarehouseSelectionListSuccessfully = (res) => {
    //returning an action
    return ({
        type: ActionTypes.FETCH_WAREHOUSE_SELECTION,
        payload: res
    });
}

export const fetchWarehouseSelectionListFailed = (errMess) => {
    //returning an action
    return ({
        type: ActionTypes.WAREHOUSE_SELECTION_FAILED,
        payload: errMess
    });
}
