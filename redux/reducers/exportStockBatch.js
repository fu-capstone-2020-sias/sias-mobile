import * as ActionTypes from "../actions/ActionTypes";

export const ExportStockBatch = (state = {
    errMess: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.EXPORT_STOCK_BATCH_SUCCESS:
            return {
                ...state,
                errMess: null,
                successStatus: true,
                successMessage: action.payload
            }
        case ActionTypes.EXPORT_ORDER_BATCH_FAILED:
            return {
                ...state,
                errMess: action.payload, // array of error messages
                successStatus: false,
                successMessage: null
            }
        case ActionTypes.CLEAR_EXPORT_STOCK_BATCH_SUCCESS:
            return {
                errMess: null,
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}