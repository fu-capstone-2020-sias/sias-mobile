import * as ActionTypes from '../actions/ActionTypes';

export const OrderDetail = (state = {
    errMess: null,
    order: null,
    items: null,
    successStatus: false,
    addedSuccessStatus: false,
    addedOrderId: null,
    addItemsSuccess: {
        status: false,
        msg: null
    },
    modifyItemsSuccess: {
        status: false,
        msg: null
    },
    deleteItemsSuccess: {
        status: false,
        msg: null
    }
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_ORDER_DETAIL:
            return { ...state, isLoading: false, errMess: null, order: action.payload };

        case ActionTypes.ORDER_DETAIL_FAILED:
            return { ...state, isLoading: false, errMess: action.payload };

        case ActionTypes.FETCH_ORDER_ITEMS:
            return { ...state, isLoading: false, errMess: null, items: action.payload };

        case ActionTypes.ORDER_ITEMS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload };

        case ActionTypes.MARK_DONE_ORDER:
            return { ...state, errMess: null, successStatus: true }

        case ActionTypes.MODIFY_ORDER_DETAIL:
            return { ...state, errMess: null, successStatus: true }

        case ActionTypes.ADD_ORDER:
            return { ...state, errMess: null, addedSuccessStatus: true, addedOrderId: action.payload.newOrder.orderId }

        case ActionTypes.ADD_ORDER_ITEMS:
            return {
                ...state,
                addItemsSuccess: {
                    status: true,
                    msg: null
                }
            };

        case ActionTypes.MODIFY_ORDER_ITEMS:
            return {
                ...state,
                modifyItemsSuccess: {
                    status: true,
                    msg: null
                }
            };

        case ActionTypes.DELETE_ORDER_ITEMS:
            return {
                ...state,
                deleteItemsSuccess: {
                    status: true,
                    msg: null
                }
            };

        case ActionTypes.SUBMIT_MODIFY_ITEMS_FAILED:
            return {
                ...state,
                errMess: action.payload,
                addItemsSuccess: {
                    status: false,
                    msg: action.payload
                },
                modifyItemsSuccess: {
                    status: false,
                    msg: action.payload
                },
                deleteItemsSuccess: {
                    status: false,
                    msg: action.payload
                }
            }

        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, successStatus: false, addedSuccessStatus: false, addedOrderId: null }

        default:
            return state;
    }
};

export const CurrentlyViewedOrder = (state = {
    currentOrderId: 0,
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_ORDER_DETAIL:
            return { ...state, currentOrderId: action.payload.orderId };

        case ActionTypes.ORDER_DETAIL_FAILED:
            return { ...state, currentOrderId: 0 };

        default:
            return state;
    }
};