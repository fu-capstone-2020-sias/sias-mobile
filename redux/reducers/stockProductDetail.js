import * as ActionTypes from '../actions/ActionTypes';

export const StockProductDetail = (state = {
    errMess: null,
    stockProduct: null,
    successStatus: false,
    addedSuccessStatus: false,
    addedStockProductId: null
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_STOCK_PRODUCT_DETAIL:
            return {...state, isLoading: false, errMess: null, stockProduct: action.payload};

        case ActionTypes.STOCK_PRODUCT_DETAIL_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        case ActionTypes.MODIFY_STOCK_PRODUCT_DETAIL:
            return {...state, errMess: null, successStatus: true}

        case ActionTypes.ADD_STOCK_PRODUCT:
            return {
                ...state,
                errMess: null,
                addedSuccessStatus: true,
                addedStockProductId: action.payload.stockProductId
            }

        case ActionTypes.CLEAR_STOCK_PRODUCT_DETAIL:
            return {
                errMess: null,
                stockProduct: null,
                successStatus: false,
                addedSuccessStatus: false,
                addedStockProductId: null
            }

        case ActionTypes.SUBMIT_FAILED:
            return {
                ...state,
                errMess: action.payload,
                successStatus: false,
                addedSuccessStatus: false,
                addedStockProductId: null
            }

        default:
            return state;
    }
};

export const CurrentlyViewedStockProduct = (state = {
    currentStockProductId: 0,
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_STOCK_PRODUCT_DETAIL:
            return {...state, currentStockProductId: action.payload.stockProductId};

        case ActionTypes.STOCK_PRODUCT_DETAIL_FAILED:
            return {...state, currentStockProductId: 0};

        default:
            return state;
    }
};