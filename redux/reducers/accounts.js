import * as ActionTypes from '../actions/ActionTypes';

export const Accounts = (state = {
    isLoading: true,
    errMess: null,
    accounts: null,
    currentPageNumber: 0,
    numberOfPages: 0,
    searchData: null,
    successStatus: false
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_ACCOUNTS:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                accounts: action.payload.accounts,
                numberOfPages: action.payload.numberOfPages,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.FETCH_ACCOUNTS_SEARCH:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                accounts: action.payload.accounts,
                searchData: action.payload.searchData,
                numberOfPages: action.payload.numberOfPages,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.ACCOUNTS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload, successStatus: false };

        case ActionTypes.ADD_ACCOUNT:
            return { ...state, errMess: null, successStatus: true }

        default:
            return state;
    }
};