import * as ActionTypes from "../actions/ActionTypes";

export const AddNewAccounts = (state = {
    errMess: null,
    newAccount: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.ADD_NEW_ACCOUNT_SUCCESS:
            return {
                ...state,
                errMess: null,
                successStatus: true,
                successMessage: action.payload
            }
        case ActionTypes.ADD_NEW_ACCOUNT_FAILED:
            return {
                ...state,
                errMess: action.payload, // array of error messages
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}