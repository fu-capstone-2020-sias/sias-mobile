import * as ActionTypes from "../actions/ActionTypes";

export const FetchCuttingInstructions = (state = {
    errMess: null,
    cuttingInstruction: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.FETCH_CUTTING_INSTRUCTION_SUCCESS:
            return {
                ...state,
                cuttingInstruction: action.payload,
                successStatus: true
            }
        case ActionTypes.FETCH_CUTTING_INSTRUCTION_FAILED:
            console.log(action.payload);
            return {
                errMess: action.payload,
                cuttingInstruction: null,
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}