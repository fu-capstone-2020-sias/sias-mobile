import * as ActionTypes from "../actions/ActionTypes";

export const AccountInfos = (state = {
    errMess: null,
    accountInfo: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.FETCH_ACCOUNT_INFO:
            return {
                ...state,
                accountInfo: action.payload,
                successStatus: true
            }
        case ActionTypes.ACCOUNT_INFO_FAILED:
            console.log("ActionTypes.ACCOUNT_INFO_FAILED");
            console.log(action.payload);
            return {
                errMess: action.payload,
                accountInfo: null,
                successStatus: false,
                successMessage: null
            }
        case ActionTypes.MODIFY_ACCOUNT_INFO_SUCCESS:
            return {
                ...state,
                errMess: null,
                successStatus: true,
                successMessage: action.payload
            }
        case ActionTypes.MODIFY_ACCOUNT_INFO_FAILED:
            return {
                ...state,
                errMess: action.payload, // array of error messages
                successStatus: false,
                successMessage: null
            }
        case ActionTypes.CLEAR_ACCOUNT_INFO_ERRMESS:
            return {
                ...state,
                errMess: null, // array of error messages
                successStatus: false,
                successMessage: null,
            }

        default:
            return state;
    }
}